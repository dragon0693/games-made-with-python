# Lista de módulos importados para el correcto funcionamiento del programa
# Módulo telnetlib: permite establecer la conexión Telnet e introducir los comandos
import telnetlib
# Módulo time: permite establecer un tiempo entre la escritura de comandos
# para que no se sobrecargue el CLI. Cada escritura de un comando irá seguida
# de un time.sleep().
import time
# Módulo math: permite realizar operaciones matemáticas con los anchos de banda
import math
# Módulos os y path: permite comprobar la existencia de ficheros en el directorio de trabajo
import os.path as path
import os
# Módulo necesario para la creación y modificación del fichero XML. Este fichero permitirá
# recuperar las configuraciones de las ONUs al apagar la red o al cambiar de modo de
# gestión (hay que recordar que el CLI NO TIENE PERSISTENCIA en los datos configurados).
import xml.etree.cElementTree as etree

# ¡¡¡¡¡ IMPORTANTE !!!!!!!!!
# El campo que permite diferenciar a las ONUs es el número de serie, que está 
# formado por los campos Vendor ID y Vendor Specific (este a su vez está formado 
# por los últimos 8 valores hexadecimales de la dirección MAC). Por tanto, lo 
# que diferencia a los números de serie de las ONUs de la red es la dirección MAC.
# Por simplicidad, en los comentarios de todo el programa se utilizará el término
# dirección MAC para referirse al número de serie.


# Función get_ID_ONU(): permite detectar las ONUs conectadas a la red GPON a través
# de su dirección MAC. Asimismo, asocia a cada dirección MAC el identficador de ONU,
# parámetro muy importante en el resto del programa y que varía cada vez que se enciende
# la red o se activa el modo de gestión CLI. También se crea en esta función, en caso de
# no existir, el fichero XML que permite cargar configuraciones anteriores
def get_ID_ONU():
    
    # Host y puerto al que se hace la conexión Telnet para acceder al CLI                    
    host = "172.26.128.38"
    port = "4551"
     
    # Claves de acceso al CLI
    password1 = "TLNT25"
    password2 = "TLNT145"
    enable = "enable"
    
    # Acceso al CLI: conexión Telnet al host y puerto indicados anteriormente
    tn = telnetlib.Telnet(host,port,1)
    # Mediante la función write de telnetlib, escritura de los comandos que permiten
    # acceder al menú de privilegios del CLI
    tn.write(password1.encode('ascii') + b"\n")      
    time.sleep(0.1)     
    tn.write(enable.encode('ascii') + b"\n")
    time.sleep(0.1)     
    tn.write(password2.encode('ascii') + b"\n")
    time.sleep(0.1) 
    
    # Declaración y escritura del comando que permite ver las ONUs conectadas a 
    # la red así como sus direcciones MAC
    comandos = "configure \n olt-device 0 \n olt-channel 0 \n show serial-number allocated \n"   
    tn.write(comandos.encode('ascii') + b"\n") 
    time.sleep(0.1)
    
    # Lectura de los datos (tanto enviados como recibidos) del CLI y volcado en 
    # un fichero de texto para su posterior análisis
    data = tn.read_very_eager().decode()
    # Se abre el fichero con modo de escritura, de esta forma cada vez que cambie
    # el estado de la red el fichero se sobreescribirá con la información nueva
    outfile = open('IDs_ONUs.txt', 'w')
    # Se escriben los datos procedentes de la escritura de los comandos de arriba
    outfile.write(data)
    # Ciere del fichero
    outfile.close()
    
    # Creación del vector que almacenará las direcciones MAC de las ONUs conectadas
    MAC = []
    # Se abre el archivo anterior en modo lectura
    outfile = open('IDs_ONUs.txt', 'r')
    # Se almacenan todas las lineas del fichero con la función readlines()
    lines = outfile.readlines()
    # Bucle que recorre cada línea del fichero
    for line in lines:
        # Se almacenan todas las palabras de cada línea
        palabras = line.split()
        # Bucle que recorre cada palabra de la línea
        for p in palabras:
            # Si los 18 primeros caracteres de una palabra coinciden con los indicados,
            # se trata de una direccion MAC -> Se ha detectado una ONU
            if p[:18]=='54-4c-52-49-5b-01-':
                # Se añade la MAC al vector. La posición en la que se añade indica
                # el identificador de la ONU.
                MAC.append(p)

#            FORMA DE BUSCAR ONUs ONU A ONU -> Menos eficiente y hay que añadir código
#            si se conecta alguna ONU más a la red 
#            if p=='54-4c-52-49-5b-01-f6-90':  
#                MAC[id_ONU] = '54-4c-52-49-5b-01-f6-90'
#                id_ONU = id_ONU + 1
#            if p=='54-4c-52-49-5b-01-f7-30':
#                MAC[id_ONU] = '54-4c-52-49-5b-01-f7-30'
#                id_ONU = id_ONU + 1
#            if p=='54-4c-52-49-5b-01-f6-d8':
#                MAC[id_ONU] = '54-4c-52-49-5b-01-f6-d8'
#                id_ONU = id_ONU + 1
#            if p=='54-4c-52-49-5b-01-f7-28':
#                MAC[id_ONU] = '54-4c-52-49-5b-01-f7-28'
#                id_ONU = id_ONU + 1
    
    # Cierre del archivo                          
    outfile.close()
    
    # Si existe un fichero con este nombre en el directorio de trabajo, sale de la función
    # devolviendo el vector de direcciones MAC
    if path.exists("configuracionGPON.xml"):
        return MAC
    
    # Si no existe, se ha de crear el fichero XML        
    else:
        # Se define el elemento raíz
        root = etree.Element("RedGPON") 
        
        # Se añaden las ONUs (el nº de ONUs es la longitud del vector de direcciones MAC)
        i=0
        while i<len(MAC):
            # Cada ONU se añade con su dirección MAC como un subelemento del elemento raíz
            ONU = etree.SubElement(root,"ONU", MAC=MAC[i])
            i=i+1

#            FORMA DE CREAR EL ÁRBOL ONU A ONU -> Menos eficiente y hay que añadir código
#            si se conecta alguna ONU más a la red 
#            ONU1 = etree.SubElement(root,"ONU", MAC='54-4c-52-49-5b-01-f6-90')               
#            ONU2 = etree.SubElement(root,"ONU", MAC='54-4c-52-49-5b-01-f7-30')
#            ONU3 = etree.SubElement(root,"ONU", MAC='54-4c-52-49-5b-01-f6-d8')
#            ONU4 = etree.SubElement(root,"ONU", MAC='54-4c-52-49-5b-01-f7-28')       
            
        # Se crea el árbol XML y se escribe en el fichero con el nombre indicado
        tree = etree.ElementTree(root)
        tree.write("configuracionGPON.xml")  
        # Se devuelve el vector de direcciones MAC
        return MAC

# Función servicio_Internet(ID_ONU,MAC_ONU,num_servicios_Internet): permite configurar
# servicios de Internet y toma como parámetros el identificador de la ONU 
# (posición en el vector de direcciones MAC), la MAC y el número de servicios a configurar. 
# Guarda la configuración actual en un fichero txt (esta configuración es temporal y se 
# mantiene mientras no se apague la red ni se cambie al modo de gestión TGMS). 
# La función también actualiza el fichero XML para recuperar configuraciones
# cuando se apaga la red o se cambia al TGMS.                                                                                 
def servicio_Internet(ID_ONU,MAC_ONU,num_servicios_Internet):
   
    # Creación de los vectores en los que se almacenarán los parámetros 
    # internos de configuración
    port_ID = []
    alloc_ID = [] 
    tcont_ID = []
    num_instancia = []
    puntero = []
    ds_profile_index = []

    # Dependiendo del número de servicios, se asginan tantos valores como sean necesarios
    # a los vectores anteriormente creados
    i=0
    while i < num_servicios_Internet: 
        # Para que no se solapen puertos y allocs-ID, se asignan en función del identficiador
        # de la ONU y del número de servicio en cuestión
        port_ID.append(600+100*ID_ONU+i)
        alloc_ID.append(600+100*ID_ONU+i) 
        # Estos valores se asignan de este modo también para evitar solapamientos
        num_instancia.append(i+2)
        tcont_ID.append(i)
        puntero.append(32768+i)
        ds_profile_index.append(i)
        i=i+1 
    
    # Flags que marcarán la salida de los diferentes bucles cuando el parámetro 
    # requerido haya sido introducido de forma válida
    true_VLAN = 0
    true_BW_Downstream_GR = 0
    true_BW_Downstream_Excess = 0
    true_BW_Upstream_GR = 0
    true_BW_Upstream_BE = 0 
    
    # Creación de los vectores en los que se almacenarán los parámetros 
    # de configuracion que el USUARIO deberá introducir: identificador de VLAN,
    # ancho de banda Downstream garantizado y en exceso y ancho de banda Upstream
    # garantizado y Best Effort
    VLAN_ID = []
    BW_Downstream_GR = []
    BW_Downstream_Excess = []
    BW_Upstream_GR = []
    BW_Upstream_BE = []

    # En los sucesivos bucles, se irán pidiendo los parámetros de configuración al usuario.
            
    # Primer bucle: se piden el identificador VLAN que corresponda a cada servicio
    # La VLAN 833 conecta con un servidor DHCP que asigna la dirección mientras que 
    # la VLAN 806 recibe una IP de forma estática (no la tiene que introducir el usuario)
    i=0
    while i < num_servicios_Internet:    
        num_servicio = str(i+1)
        true_VLAN = 0
        cont=0
        # Las etiquetas VLAN van de 1 a 4094. Si el usuario introduce un valor fuera de ese
        # rango, se le volverá a pedir que introduzca el valor. Hay que recordar que la red
        # solo ofrece servicio en las VLAN 833 y 806    
        while true_VLAN == 0: 
            if cont == 0:
                print("Identficador VLAN [1-4094] para el servicio " + num_servicio + " (833 - Servidor DHCP, 806 - Dirección IP estática): ")
            else:
                print("El identificador VLAN no es válido. Vuelva a probar:")
            cont = cont+1
            entrada = input()
            entrada = int(entrada)            
            if entrada > 0 and entrada < 4095:
                VLAN_ID.append(entrada)
                print("El identificador VLAN para el servicio " + num_servicio + " es:", VLAN_ID[i])                 
                i=i+1                
                true_VLAN = 1                              
    
    print("\n")
    
    # Segundo bucle: se pide el ancho de banda garantizado en sentido Downstream.
    # Este ancho de banda se introduce en Kbps y debe estar entre 0 y 2488000 (aunque
    # a efectos prácticos no tiene sentido meter más de 600000 Kbps, aproximadamente). 
    # El CLI solo admite múltiplos de 64 Kbps de manera que el programa trunca el 
    # valor del usuario para que coincida con un múltiplo de 64 Kbps.
    i=0    
    while i < num_servicios_Internet:
        num_servicio = str(i+1)
        true_BW_Downstream_GR = 0
        cont=0
        while true_BW_Downstream_GR == 0:
            if cont == 0:
                print("Ancho de banda Downstream garantizado en Kbps [0-2488000] para el servicio " + num_servicio + ": ")
            else:
                print("Ancho de banda no válido. Vuelva a probar:")
            cont=cont+1
            entrada = input()
            entrada = int(entrada)
            entrada = entrada / 64
            entrada = math.floor(entrada)
            entrada = entrada * 64
            if entrada > -1 and entrada < 2488000:
                BW_Downstream_GR.append(entrada)
                print("El ancho de banda Downstream garantizado en Kbps es:", BW_Downstream_GR[i])
                i=i+1                
                true_BW_Downstream_GR = 1
    
    # Tercer bucle: se pide el ancho de banda en exceso en sentido Downstream.
    # Este ancho de banda se introduce en Kbps y debe estar entre 0 y 2488000 (aunque
    # a efectos prácticos no tiene sentido que la suma del garantizado y exceso sea
    # mayor que 600000 Kbps, aproximadamente). El CLI solo admite múltiplos de 64 Kbps 
    # de manera que el programa trunca el valor del usuario para que coincida con
    # un múltiplo de 64 Kbps.
    i=0    
    while i < num_servicios_Internet:
        num_servicio = str(i+1)
        true_BW_Downstream_Excess = 0
        cont=0
        while true_BW_Downstream_Excess == 0:
            if cont == 0:
                print("Ancho de banda Downstream en exceso en Kbps [0-2488000] para el servicio " + num_servicio + ": ")
            else:
                print("Ancho de banda no válido. Vuelva a probar:")
            cont=cont+1
            entrada = input()
            entrada = int(entrada)
            entrada = entrada / 64
            entrada = math.floor(entrada)
            entrada = entrada * 64
            if entrada > -1 and entrada < 2488000:
                BW_Downstream_Excess.append(entrada)
                print("El ancho de banda Downstream en exceso en Kbps es:", BW_Downstream_Excess[i])       
                i=i+1                
                true_BW_Downstream_Excess = 1
                                     
    print("\n")
     
    # Cuarto bucle: se pide el ancho de banda garantizado en sentido Upstream.
    # Este ancho de banda se introduce en Mbps y debe estar entre 0 y 1244 (aunque
    # a efectos prácticos no tiene sentido meter más de 600 Mbps, aproximadamente).         
    i=0
    while i < num_servicios_Internet:    
        num_servicio = str(i+1)
        true_BW_Upstream_GR = 0
        cont=0
        while true_BW_Upstream_GR == 0: 
            if cont == 0:
                print("Ancho de banda Upstream garantizado en Mbps [0-1244] para el servicio " + num_servicio + ": ")
            else:
                print("Ancho de banda no válido. Vuelva a probar:")
            cont=cont+1
            entrada = input()
            entrada = int(entrada)            
            if entrada > -1 and entrada < 1245:
                BW_Upstream_GR.append(entrada)
                print("El ancho de banda Upstream garantizado en Mbps es:", BW_Upstream_GR[i])                 
                i=i+1
                true_BW_Upstream_GR = 1                              
                
    print("\n")
    
    # Quinto bucle: se pide el ancho de banda garantizado Best Effort Upstream.
    # Este ancho de banda se introduce en Mbps, debe estar entre 0 y 1244 (aunque
    # a efectos prácticos no tiene sentido meter más de 600 Mbps, aproximadamente) y 
    # tiene que ser igualo mayor que el ancho de banda Upstream garantizado (el valor Best 
    # Effort es el mayor ancho de banda que podrá recibir la ONU)                                                                       
    i=0
    while i < num_servicios_Internet:    
        num_servicio = str(i+1)
        true_BW_Upstream_BE = 0
        cont=0
        while true_BW_Upstream_BE == 0:  
            if cont == 0:
                print("Ancho de banda Upstream BE en Mbps [0-1244] para el servicio " + num_servicio + ": ")
            else:
                print("Ancho de banda no válido. Vuelva a probar:")
            cont=cont+1
            entrada = input()
            entrada = int(entrada)            
            if entrada > -1 and entrada < 1245 and entrada >= BW_Upstream_GR[i]:
                BW_Upstream_BE.append(entrada)
                print("El ancho de banda Upstream BE en Mbps es:", BW_Upstream_BE[i])                 
                i=i+1               
                true_BW_Upstream_BE = 1                              
    
    print("\n")
                
    # Host y puerto al que se hace la conexión Telnet para acceder al CLI                    
    host = "172.26.128.38"
    port = "4551"
     
    # Claves de acceso al CLI
    password1 = "TLNT25"
    password2 = "TLNT145"
    enable = "enable"
    
    # Acceso al CLI: conexión Telnet al host y puerto indicados anteriormente
    tn = telnetlib.Telnet(host,port,1)
    # Mediante la función write de telnetlib, escritura de los comandos que permiten
    # acceder al menú de privilegios del CLI
    tn.write(password1.encode('ascii') + b"\n")      
    time.sleep(0.1)     
    tn.write(enable.encode('ascii') + b"\n")
    time.sleep(0.1)     
    tn.write(password2.encode('ascii') + b"\n")
    time.sleep(0.1) 
    
    # A la hora de introducir variables en la declaración de cadenas, aunque sean números,
    # se deben introducir en forma de string. Por ello, aparece a lo largo del programa muchas
    # ocasiones la función str(), que convierte una variable en string
    ID_ONU = str(ID_ONU)
    
    # A continuación, se definen todos los comandos necesarios para dar el servicio de Datos.
    # Posteriormente serán ejecutados en el CLI con la función write.
    # Primero se crea el canal OMCI de comunicación (con el mismo identificador que el de la ONU por convención)
    # Se resetean las entidades MIB que pudiera haber y se activa fec en uplink (pasos opcionales)
    inicio = "configure \n olt-device 0 \n olt-channel 0 \n onu-local " + ID_ONU + " \n omci-port  " + ID_ONU + "  \n exit \n onu-omci  " + ID_ONU + "  \n ont-data mib-reset \n exit \n fec direction uplink  " + ID_ONU + "  \n onu-local  " + ID_ONU + "  \n"                
    tn.write(inicio.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    i=0
    # Se crean, dentro del menú de la ONU a configurar, tantos Alloc-ID como servicios
    while i < num_servicios_Internet: 
        allocID = "alloc-id " + str(alloc_ID[i]).strip('[]') + " \n"
        tn.write(allocID.encode('ascii') + b"\n")
        time.sleep(0.2) 
        i=i+1
    
    # El comando exit hace salir hacia el menú anterior del CLI en la estructura de menús  
    salir = "exit \n"
    tn.write(salir.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Cada Alloc-ID irá asociado a un puerto (por convención, se utiliza el mismo identificador)
    # Estos Alloc-IDs y puertos no se pueden utilizar para otras ONUs ni para otros servicios
    i=0
    while i < num_servicios_Internet:
        portalloc = "port " + str(port_ID[i]).strip('[]') + " alloc-id  " + str(alloc_ID[i]).strip('[]') + " \n"
        tn.write(portalloc.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
   
    # Desde el menú de configuración del canal OMCI de comunicación, se crean las entidades
    # MIB que forman el servicio de Internet. Estas entidades vienen en el estándar GPON.    
    omci = "onu-omci " + ID_ONU + " \n"
    tn.write(omci.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Creación de las entidades T-Cont (colas). Cada servicio está asociado con un T-Cont 
    # y está vinculado a un Alloc-ID
    i=0
    while i < num_servicios_Internet:
        tcont = "t-cont set slot-id 128 t-cont-id " + str(tcont_ID[i]).strip('[]') + " alloc-id " + str(alloc_ID[i]).strip('[]') + "  \n"
        tn.write(tcont.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1       
    
    # Creación de MAC Brigde Service Profile. Se asociará esta entidad con los MAC Bridge 
    # Port Configuration Data a través del bridge-group-id en los siguientes comandos.
    macservice = "mac-bridge-service-profile create slot-id 0 bridge-group-id 1 spanning-tree-ind true learning-ind true atm-port-bridging-ind true priority 32000 max-age 1536 hello-time 256 forward-delay 1024 unknown-mac-address-discard false mac-learning-depth 255 dynamic-filtering-ageing-time 1000 \n"
    # Creación del primer MAC Bridge Port Configuration Data. Irá asociado a la entidad 
    # extended-vlan-tagging-operation-config-data. Para ello, el tp-type (tipo del punto 
    # de terminación del MAC Bridge) ha de ser lan y el puntero tp-ptr debe tener el mismo 
    # valor que el nº de instancia de la entidad extended-vlan-tagging-operation-config-data.
    macbridge1 = "mac-bridge-pcd create instance 1 bridge-id-ptr 1 port-num 1 tp-type lan tp-ptr 257 port-priority 2 port-path-cost 32 port-spanning-tree-ind true encap-method llc lanfcs-ind forward \n"
    tn.write(macservice.encode('ascii') + b"\n")
    time.sleep(0.2)
    tn.write(macbridge1.encode('ascii') + b"\n")
    time.sleep(0.2) 
    
    # Creación de los restantes Mac Brigde Port Configuration Data. Irán asociado a la entidades 
    # VLAN-tagging-filter-data mediante los números de instancia. También irán asociados a los 
    # GEM Interworking Termination Point mediante el tp-type (tipo gem) y el tp-ptr, que
    # tiene que coincidir con el número de instancia del GEM Interworking Termination Point
    i=0
    while i < num_servicios_Internet:   
        macbridge2 = "mac-bridge-pcd create instance " + str(num_instancia[i]).strip('[]') + " bridge-id-ptr 1 port-num " + str(num_instancia[i]).strip('[]') + " tp-type gem tp-ptr " + str(num_instancia[i]).strip('[]') + " port-priority 0 port-path-cost 1 port-spanning-tree-ind true encap-method llc lanfcs-ind forward  \n"
        tn.write(macbridge2.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # Creación de los GEM Port Network CTP, que irán asociado a los puertos especificados 
    # anteriormente. Los identificadores tienen que coincidir con el gem-port-nwk-ct-conn-ptr 
    # de los GEM Interworking Termination Point.
    i=0
    while i < num_servicios_Internet:    
        gemport = "gem-port-network-ctp create instance " + str(num_instancia[i]).strip('[]') + " port-id  " + str(port_ID[i]).strip('[]') + "  t-cont-ptr " + str(puntero[i]).strip('[]') + " direction bidirectional traffic-mgnt-ptr-ustream 0 traffic-descriptor-profile-ptr 0 priority-queue-ptr-downstream 0 traffic-descriptor-profile-ds-ptr 0 enc-key-ring 0 \n"
        tn.write(gemport.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # Creación de los GEM Interworking Termination Point (en este punto, se produce la 
    # transformación de flujo de bytes a tramas GEM y viceversa). Estas entidades se vinculan 
    # a los GEM Port Network CTP a través del campo gem-port-nwk-ctp-conn-ptr, que debe coincidir 
    # con el número de instancia que  utilizado en el GEM Port Network CTP. Estas entidades también 
    # se asocian con los MAC Bridge Port Configuration. Para ello, hay que seleccionar como 
    # interwork-option mac-bridge-lan y el campo service-profile-ptr debe ser un 1. 
    # El número de instancia debe ser el mismo que el tp-ptr del MAC Bridge Point Configuration Data.     
    i=0
    while i < num_servicios_Internet:
        geminterworking = "gem-interworking-termination-point create instance " + str(num_instancia[i]).strip('[]') + " gem-port-nwk-ctp-conn-ptr " + str(num_instancia[i]).strip('[]') + " interwork-option mac-bridge-lan service-profile-ptr 1 interwork-tp-ptr 0 gal-profile-ptr 0 \n"
        tn.write(geminterworking.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1

    # Creación de los VLAN Tagging Filter Data con los identficadores VLAN definidos arriba. 
    # Los números de instancia deben coincidir con los de los MAC Bridge Port Configuration Data asociados.
    i=0
    while i < num_servicios_Internet:
        vlantagging = "vlan-tagging-filter-data create instance " + str(num_instancia[i]).strip('[]') + "  forward-operation h-vid-a vlan-tag1 " + str(VLAN_ID[i]).strip('[]') + " vlan-priority1 7  vlan-tag2 null vlan-priority2 null vlan-tag3 null vlan-priority3 null vlan-tag4 null vlan-priority4 null vlan-tag5 null vlan-priority5 null vlan-tag6 null vlan-priority6 null vlan-tag7 null vlan-priority7 null vlan-tag8 null vlan-priority8 null vlan-tag9 null vlan-priority9 null vlan-tag10 null vlan-priority10 null vlan-tag11 null vlan-priority11 null vlan-tag12 null vlan-priority12 null \n"
        tn.write(vlantagging.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1

    # Creación de la entidad Extended VLAN Tagging Operation Config Data, que será configurada en el paso posterior. 
    # Sirve para gestionar los identificadores VLAN. Esta entidad está asociada al primer MAC Bridge Port Configuration Data 
    # a través del número de instancia, que coincide con el tp-ptr del MAC Bridge Port Configuration Data.
    extendedvlan = "extended-vlan-tagging-operation-config-data create instance 257 association-type pptp-eth-uni associated-me-ptr 257 \n"
    tn.write(extendedvlan.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Configuración de la entidadExtended VLAN Tagging Operation Config Data. Se debe configurar
    # para cada identificador VLAN.
    i=0
    while i < num_servicios_Internet:
        extendedvlanconf = "extended-vlan-tagging-operation-config-data set instance 257 operations-entry filter-outer-prio filter-prio-no-tag filter-outer-vid none filter-outer-tpid none filter-inner-prio filter-prio-none filter-inner-vid " + str(VLAN_ID[i]).strip('[]') + " filter-inner-tpid none filter-ethertype none treatment-tag-to-remove 1 treatment-outer-prio none treatment-outer-vid copy-from-inner treatment-outer-tpid tpid-de-copy-from-outer treatment-inner-prio 0 treatment-inner-vid " + str(VLAN_ID[i]).strip('[]') + " treatment-inner-tpid tpid-de-copy-from-inner\n"
        tn.write(extendedvlanconf.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # El comando exit hace salir hacia el menú anterior del CLI en la estructura de menús  
    salir = "exit \n"
    tn.write(salir.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Configuración de las reglas VLAN en la OLT asociadas a los puertos definidos al principio de la función
    i=0
    while i < num_servicios_Internet:
        reglasvlan = "vlan uplink configuration port-id " + str(port_ID[i]).strip('[]') + " min-cos 0 max-cos 7 de-bit disable primary-tag-handling false \n vlan uplink handling port-id  " + str(port_ID[i]).strip('[]') + "  primary-vlan none destination datapath c-vlan-handling no-change s-vlan-handling no-change new-c-vlan 0 new-s-vlan 0 \n"
        tn.write(reglasvlan.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # Creación de los perfiles de ancho de banda en sentido Downstream con los parámetros 
    # definidos por el usuario anteriormente. 
    i=0
    while i < num_servicios_Internet:
        perfildownstreamconf = "policing downstream profile committed-max-bw " + str(BW_Downstream_GR[i]).strip('[]') + " committed-burst-size 1023 excess-max-bw " + str(BW_Downstream_Excess[i]).strip('[]') + " excess-burst-size 1023 \n"
        tn.write(perfildownstreamconf.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # Nombre del fichero en el que se guardará la configuración del servicio de Internet            
    nombre_archivo = 'Servicio_Internet_ONU_MAC_' + MAC_ONU + '.txt'
    
    # Al crear los perfiles de  ancho de banda en sentido de bajada, el CLI devuelve un 
    # índice de perfil. Para asociar los perfiles creados, se deben utilizar esos índices
    # de perfil y asociarlos a los puertos definidos anteriormente. Para ello, se vuelcan los datos
    # enviados y recibidos del CLI en un fichero, del que se extraerán esos índices.
    datos_perfil = tn.read_very_eager().decode()
    outfile = open(nombre_archivo, 'a')
    outfile.write(datos_perfil)
    outfile.close()
    outfile = open(nombre_archivo, 'r')             
    lines = outfile.readlines()
    true_ds_profile = 0
    # Se busca el índice de perfil que primero aparezca en el fichero.
    i=0
    for i in range (0,500):
        if true_ds_profile == 1:
            break                
        j = str(i)
        cadena = 'downstream_profile_index: ' + j + ' '
        for line in lines:
            if cadena in line:
                j = int(j)
                ds_profile_index[0] = j
                true_ds_profile = 1            
    
    outfile.close()
    
    # El índice encontrado corresponde al primer servicio. En caso de haber más, el índice
    # de perfil será ese aumentado en una unidad.
    i=0
    for i in range (0,num_servicios_Internet):
        ds_profile_index[i] = ds_profile_index[0] + i
       
    # Asignación de los perfiles de ancho de banda Downstream a los puertos correspondientes
    # mediante los índices de perfil buscados anteriormente.
    i=0
    while i < num_servicios_Internet:
        perfildownstreamassign = "policing downstream port-configuration entity port-id " + str(port_ID[i]).strip('[]') + " ds-profile-index " + str(ds_profile_index[i]).strip('[]') + " \n"    
        tn.write(perfildownstreamassign.encode('ascii') + b"\n")    
        time.sleep(0.2)
        i=i+1
    
    # El comando exit hace salir hacia el menú anterior del CLI en la estructura de menús  
    salir = "exit \n"
    tn.write(salir.encode('ascii') + b"\n")
    time.sleep(0.2)

    # Desde el menú de configuración del algoritmo DBA, se definirián los perfiles de
    # ancho de banda en sentido Upstream
    dba = "pon \n dba pythagoras 0 \n "
    tn.write(dba.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Cada perfil upstream irá asociado a un Alloc-ID y estará configurado con los
    # parámetros que haya definido el usuario.
    i=0
    while i < num_servicios_Internet:
        perfilupstream = "sla " + str(alloc_ID[i]).strip('[]') + " service data status-report nsr gr-bw " + str(BW_Upstream_GR[i]).strip('[]') + " gr-fine 0 be-bw " + str(BW_Upstream_BE[i]).strip('[]') + " be-fine 0 \n"     
        tn.write(perfilupstream.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1

    # El comando end hace salir directamente al modo privilegiado en la estructura
    # de menús del CLI
    final = "end \n"
    tn.write(final.encode('ascii') + b"\n")
    time.sleep(0.2)         
    
    # Se vuelcan todos los datos en el fichero definido anteriormente (la opción 'a'
    # hace que los datos se añadan al final del fichero) de forma que el fichero recogerá
    # toda la configuración del servicio de Internet
    data = tn.read_very_eager().decode() 
    outfile = open(nombre_archivo, 'a')
    outfile.write(data)
    outfile.write("\n\n\n")
    outfile.close()
    
    # Una vez configurado el servicio, se muestra un mensaje al usuario            
    print("Servicio de Internet configurado. \n")
    
    # Actualización del fichero XML con los datos configurados 
    doc = etree.parse("configuracionGPON.xml")
    # Extracción del elemento raíz
    redGPON = doc.getroot()
    
    # Obtención del índice correspondiente a la ONU que se está configurando.
    # Para ello, se hace una comparación de las diferentes direcciones MAC con 
    # la de la ONU a configurar.
    index=0
    true_index=0
    while true_index==0:
        for attr,value in redGPON[index].items():
            if value == MAC_ONU:
                true_index=1
                break
            index=index+1
    
    # Se borra cualquier configuración anterior que en este caso, al configurar un
    # nuevo servicio, se va a desechar
    num_servicios = len(redGPON[index])
    i=0
    while i<num_servicios:
        redGPON[index].remove(redGPON[index][0])
        i=i+1
    
    # Se actualiza la parte del árbol correspondiente a la ONU en cuestión con los
    # parámetros que ha definido el usuario. Se utiliza la función SubElement para
    # crear el servicio así como los parámetros del mismo.
    i=0
    while i < num_servicios_Internet:       
        Servicio = etree.SubElement(redGPON[index], "Servicio", tipo='Internet')
        VLAN = etree.SubElement(redGPON[index][i], "VLAN_ID").text = str(VLAN_ID[i]).strip('[]')
        BW_Down_GR = etree.SubElement(redGPON[index][i], "BW_Down_GR").text = str(BW_Downstream_GR[i]).strip('[]')                        
        BW_Down_Excess = etree.SubElement(redGPON[index][i], "BW_Down_Excess").text = str(BW_Downstream_Excess[i]).strip('[]')
        BW_Up_GR = etree.SubElement(redGPON[index][i], "BW_Up_GR").text = str(BW_Upstream_GR[i]).strip('[]')                        
        BW_Up_BE = etree.SubElement(redGPON[index][i], "BW_Up_BE").text = str(BW_Upstream_BE[i]).strip('[]')   
        i=i+1                            
    
    # Finalmente se crea el nuevo árbol actualizado y se escribe en el fichero
    doc = etree.ElementTree(redGPON)
    doc.write("configuracionGPON.xml")
                                
    return

# Función servicio_Video(ID_ONU,MAC_ONU,num_servicios_Video): permite configurar
# servicios de Internet + Vídeo (el servicio multicast del vídeo debe ir sobre uno
# Ethernet para gestionar el tráfico IMGP asociado) y toma como parámetros el identificador de la ONU 
# (posición en el vector de direcciones MAC), la MAC y el número de servicios a configurar. 
# Guarda la configuración actual en un fichero txt (esta configuración es temporal y se 
# mantiene mientras no se apague la red ni se cambie al modo de gestión TGMS). 
# La función también actualiza el fichero XML para recuperar configuraciones
# cuando se apaga la red o se cambia al TGMS.                     
def servicio_Video(ID_ONU,MAC_ONU,num_servicios_Video):
            

    # Creación de los vectores en los que se almacenarán los parámetros 
    # internos de configuración
    port_ID = []
    alloc_ID = [] 
    tcont_ID = []
    num_instancia = []
    puntero = []
    ds_profile_index = []

    # Dependiendo del número de servicios, se asginan tantos valores como sean necesarios
    # a los vectores anteriormente creados
    i=0
    while i < num_servicios_Video: 
        # Para que no se solapen puertos y allocs-ID, se asignan en función del identficiador
        # de la ONU y del número de servicio en cuestión
        port_ID.append(600+100*ID_ONU+i)
        alloc_ID.append(600+100*ID_ONU+i) 
        # Estos valores se asignan de este modo también para evitar solapamientos
        num_instancia.append(i+3)
        tcont_ID.append(i)
        puntero.append(32768+i)
        ds_profile_index.append(i)
        i=i+1 
    
    # Flags que marcarán la salida de los diferentes bucles cuando el parámetro 
    # requerido haya sido introducido de forma válida
    true_VLAN = 0
    true_BW_Downstream_GR = 0
    true_BW_Downstream_Excess = 0
    true_BW_Upstream_GR = 0
    true_BW_Upstream_BE = 0 
    
    # Creación de los vectores en los que se almacenarán los parámetros 
    # de configuracion que el USUARIO deberá introducir: identificador de VLAN,
    # ancho de banda Downstream garantizado y en exceso y ancho de banda Upstream
    # garantizado y Best Effort
    VLAN_ID = []
    BW_Downstream_GR = []
    BW_Downstream_Excess = []
    BW_Upstream_GR = []
    BW_Upstream_BE = []

    # En los sucesivos bucles, se irán pidiendo los parámetros de configuración al usuario.
    # IMPORTANTE: EN EL CLI, NO HAY QUE DEFINIR ANCHOS DE BANDA PARA EL SERVICIO MULTICAST.
    # EL ANCHO DE BANDA UTILIZADO POR EL VÍDEO ES EL DEFINIDO EN EL SERVICIO ETHERNET.
     
    # Primer bucle: se piden el identificador VLAN que corresponda a cada servicio
    # La VLAN 833 conecta con un servidor DHCP que asigna la dirección mientras que 
    # la VLAN 806 recibe una IP de forma estática (no la tiene que introducir el usuario)
    i=0
    while i < num_servicios_Video:    
        num_servicio = str(i+1)
        true_VLAN = 0
        cont=0
        # Las etiquetas VLAN van de 1 a 4094. Si el usuario introduce un valor fuera de ese
        # rango, se le volverá a pedir que introduzca el valor. Hay que recordar que la red
        # solo ofrece servicio en las VLAN 833 y 806          
        while true_VLAN == 0: 
            if cont == 0:
                print("Identficador VLAN [1-4094] para el servicio " + num_servicio + " (833 - Servidor DHCP, 806 - Dirección IP estática): ")
            else:
                print("El identificador VLAN no es válido. Vuelva a probar:")
            cont = cont+1
            entrada = input()
            entrada = int(entrada)            
            if entrada > 0 and entrada < 4095:
                VLAN_ID.append(entrada)
                print("El identificador VLAN para el servicio " + num_servicio + " es:", VLAN_ID[i])                 
                i=i+1                
                true_VLAN = 1                              
    
    print("\n")
    
    # Segundo bucle: se pide el ancho de banda garantizado en sentido Downstream.
    # Este ancho de banda se introduce en Kbps y debe estar entre 0 y 2488000 (aunque
    # a efectos prácticos no tiene sentido meter más de 600000 Kbps, aproximadamente). 
    # El CLI solo admite múltiplos de 64 Kbps de manera que el programa trunca el 
    # valor del usuario para que coincida con un múltiplo de 64 Kbps.    
    i=0    
    while i < num_servicios_Video:
        num_servicio = str(i+1)
        true_BW_Downstream_GR = 0
        cont=0
        while true_BW_Downstream_GR == 0:
            if cont == 0:
                print("Ancho de banda Downstream garantizado en Kbps [0-2488000] para el servicio " + num_servicio + ": ")
            else:
                print("Ancho de banda no válido. Vuelva a probar:")
            cont=cont+1
            entrada = input()
            entrada = int(entrada)
            entrada = entrada / 64
            entrada = math.floor(entrada)
            entrada = entrada * 64
            if entrada > -1 and entrada < 2488000:
                BW_Downstream_GR.append(entrada)
                print("El ancho de banda Downstream garantizado en Kbps es:", BW_Downstream_GR[i])
                i=i+1                
                true_BW_Downstream_GR = 1

    # Tercer bucle: se pide el ancho de banda en exceso en sentido Downstream.
    # Este ancho de banda se introduce en Kbps y debe estar entre 0 y 2488000 (aunque
    # a efectos prácticos no tiene sentido que la suma del garantizado y exceso sea
    # mayor que 600000 Kbps, aproximadamente). El CLI solo admite múltiplos de 64 Kbps 
    # de manera que el programa trunca el valor del usuario para que coincida con
    # un múltiplo de 64 Kbps.                 
    i=0    
    while i < num_servicios_Video:
        num_servicio = str(i+1)
        true_BW_Downstream_Excess = 0
        cont=0
        while true_BW_Downstream_Excess == 0:
            if cont == 0:
                print("Ancho de banda Downstream en exceso en Kbps [0-2488000] para el servicio " + num_servicio + ": ")
            else:
                print("Ancho de banda no válido. Vuelva a probar:")
            cont=cont+1
            entrada = input()
            entrada = int(entrada)
            entrada = entrada / 64
            entrada = math.floor(entrada)
            entrada = entrada * 64
            if entrada > -1 and entrada < 2488000:
                BW_Downstream_Excess.append(entrada)
                print("El ancho de banda Downstream en exceso en Kbps es:", BW_Downstream_Excess[i])       
                i=i+1                
                true_BW_Downstream_Excess = 1
           
                          
    print("\n")

    # Cuarto bucle: se pide el ancho de banda garantizado en sentido Upstream.
    # Este ancho de banda se introduce en Mbps y debe estar entre 0 y 1244 (aunque
    # a efectos prácticos no tiene sentido meter más de 600 Mbps, aproximadamente).                
    i=0
    while i < num_servicios_Video:    
        num_servicio = str(i+1)
        true_BW_Upstream_GR = 0
        cont=0
        while true_BW_Upstream_GR == 0: 
            if cont == 0:
                print("Ancho de banda Upstream garantizado en Mbps [0-1244] para el servicio " + num_servicio + ": ")
            else:
                print("Ancho de banda no válido. Vuelva a probar:")
            cont=cont+1
            entrada = input()
            entrada = int(entrada)            
            if entrada > -1 and entrada < 1245:
                BW_Upstream_GR.append(entrada)
                print("El ancho de banda Upstream garantizado en Mbps es:", BW_Upstream_GR[i])                 
                i=i+1
                true_BW_Upstream_GR = 1                              
                
    print("\n")

    # Quinto bucle: se pide el ancho de banda garantizado Best Effort Upstream.
    # Este ancho de banda se introduce en Mbps, debe estar entre 0 y 1244 (aunque
    # a efectos prácticos no tiene sentido meter más de 600 Mbps, aproximadamente) y 
    # tiene que ser igualo mayor que el ancho de banda Upstream garantizado (el valor Best 
    # Effort es el mayor ancho de banda que podrá recibir la ONU)                  
    i=0
    while i < num_servicios_Video:    
        num_servicio = str(i+1)
        true_BW_Upstream_BE = 0
        cont=0
        while true_BW_Upstream_BE == 0:  
            if cont == 0:
                print("Ancho de banda Upstream BE en Mbps [0-1244] para el servicio " + num_servicio + ": ")
            else:
                print("Ancho de banda no válido. Vuelva a probar:")
            cont=cont+1
            entrada = input()
            entrada = int(entrada)            
            if entrada > -1 and entrada < 1245 and entrada >= BW_Upstream_GR[i]:
                BW_Upstream_BE.append(entrada)
                print("El ancho de banda Upstream BE en Mbps es:", BW_Upstream_BE[i])                 
                i=i+1               
                true_BW_Upstream_BE = 1                              
    
    print("\n")
                
    # Host y puerto al que se hace la conexión Telnet para acceder al CLI                    
    host = "172.26.128.38"
    port = "4551"
     
    # Claves de acceso al CLI
    password1 = "TLNT25"
    password2 = "TLNT145"
    enable = "enable"
    
    # Acceso al CLI: conexión Telnet al host y puerto indicados anteriormente
    tn = telnetlib.Telnet(host,port,1)
    # Mediante la función write de telnetlib, escritura de los comandos que permiten
    # acceder al menú de privilegios del CLI
    tn.write(password1.encode('ascii') + b"\n")      
    time.sleep(0.1)     
    tn.write(enable.encode('ascii') + b"\n")
    time.sleep(0.1)     
    tn.write(password2.encode('ascii') + b"\n")
    time.sleep(0.1) 
    
    # A la hora de introducir variables en la declaración de cadenas, aunque sean números,
    # se deben introducir en forma de string. Por ello, aparece a lo largo del programa muchas
    # ocasiones la función str(), que convierte una variable en string
    ID_ONU = str(ID_ONU)
    
    # A continuación, se definen todos los comandos necesarios para dar el servicio de Datos.
    # Posteriormente serán ejecutados en el CLI con la función write.
    # Primero se crea el canal OMCI de comunicación (con el mismo identificador que el de la ONU por convención)
    # Se resetean las entidades MIB que pudiera haber y se activa fec en uplink (pasos opcionales)
    inicio = "configure \n olt-device 0 \n olt-channel 0 \n onu-local " + ID_ONU + " \n omci-port  " + ID_ONU + "  \n exit \n onu-omci  " + ID_ONU + "  \n ont-data mib-reset \n exit \n fec direction uplink  " + ID_ONU + "  \n onu-local  " + ID_ONU + "  \n"                
    tn.write(inicio.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Se crean, dentro del menú de la ONU a configurar, tantos Alloc-ID como servicios Ethernet
    # ¡¡¡IMPORTANTE!!! El servicio multicast ocupa solo el canal descendente y  no necesita la 
    # asignación de un Alloc-ID para el acceso al canal ascendente de transmisión ni parámetros de SLA.
    i=0
    while i < num_servicios_Video: 
        allocID = "alloc-id " + str(alloc_ID[i]).strip('[]') + " \n"
        tn.write(allocID.encode('ascii') + b"\n")
        time.sleep(0.2) 
        i=i+1
        
    # El comando exit hace salir hacia el menú anterior del CLI en la estructura de menús.  
    salir = "exit \n"
    tn.write(salir.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Cada Alloc-ID irá asociado a un puerto (por convención, se utiliza el mismo identificador).
    # Estos Alloc-IDs y puertos no se pueden utilizar para otras ONUs ni para otros servicios.
    # El servicio multicast irá sobre el puerto 4094 (podría ser otro) pero como se explicó arriba,
    # no necesita la asignación de un Alloc-ID
    i=0
    while i < num_servicios_Video:
        portalloc = "port " + str(port_ID[i]).strip('[]') + " alloc-id  " + str(alloc_ID[i]).strip('[]') + " \n"
        tn.write(portalloc.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # Desde el menú de configuración del canal OMCI de comunicación, se crean las entidades
    # MIB que forman el servicio de Internet y Vídeo. Estas entidades vienen en el estándar GPON.  
    omci = "onu-omci " + ID_ONU + " \n"
    tn.write(omci.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Creación de las entidades T-Cont (colas). Cada servicio Ethernet está asociado 
    # con un T-Cont y está vinculado a un Alloc-ID
    i=0
    while i < num_servicios_Video:
        tcont = "t-cont set slot-id 128 t-cont-id " + str(tcont_ID[i]).strip('[]') + " alloc-id " + str(alloc_ID[i]).strip('[]') + "  \n"
        tn.write(tcont.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1       
    
    # Creación de MAC Brigde Service Profile. Se asociará esta entidad con los MAC Bridge 
    # Port Configuration Data a través del bridge-group-id en los siguientes comandos.
    macservice = "mac-bridge-service-profile create slot-id 0 bridge-group-id 1 spanning-tree-ind true learning-ind true atm-port-bridging-ind true priority 32000 max-age 1536 hello-time 256 forward-delay 1024 unknown-mac-address-discard false mac-learning-depth 255 dynamic-filtering-ageing-time 1000 \n"
    # Creación del primer MAC Bridge Port Configuration Data. Irá asociado a la entidad 
    # extended-vlan-tagging-operation-config-data. Para ello, el tp-type (tipo del punto 
    # de terminación del MAC Bridge) ha de ser lan y el puntero tp-ptr debe tener el mismo 
    # valor que el nº de instancia de la entidad extended-vlan-tagging-operation-config-data. 
    # Esta entidad está vinculada a los servicios Ethernet.
    macbridge1 = "mac-bridge-pcd create instance 1 bridge-id-ptr 1 port-num 1 tp-type lan tp-ptr 257 port-priority 2 port-path-cost 32 port-spanning-tree-ind true encap-method llc lanfcs-ind forward \n"
    tn.write(macservice.encode('ascii') + b"\n")
    time.sleep(0.2)
    tn.write(macbridge1.encode('ascii') + b"\n")
    time.sleep(0.2) 
    
    # Creación del segundo MAC Bridge Port Configuration Data. Esta entidad está 
    # vinculada al servicio multicast. Se asocia a la entidad Multicast GEM Interworking 
    # Termination Poing. Para ello, el tipo de puntero (tp-type) ha de ser de tipo multicast 
    # (mc-gem) y el tp-ptr ha de coincidir con el nº de instancia la entidad Multicast GEM
    # Interworking Termination Poing.    
    macbridge2 = "mac-bridge-pcd create instance 2 bridge-id-ptr 1 port-num 2 tp-type mc-gem tp-ptr 2 port-priority 0 port-path-cost 1 port-spanning-tree-ind true encap-method llc lanfcs-ind forward  \n"
    tn.write(macbridge2.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Creación de los restantes Mac Brigde Port Configuration Data. Irán asociado a la entidades 
    # VLAN-tagging-filter-data mediante los números de instancia. También irán asociados a los 
    # GEM Interworking Termination Point mediante el tp-type (tipo gem) y el tp-ptr, que
    # tiene que coincidir con el número de instancia del GEM Interworking Termination Point.
    # Estas entidades están vinculadas a los servicios de tipo Ethernet.
    i=0
    while i < num_servicios_Video:   
        macbridge3 = "mac-bridge-pcd create instance " + str(num_instancia[i]).strip('[]') + " bridge-id-ptr 1 port-num " + str(num_instancia[i]).strip('[]') + " tp-type gem tp-ptr " + str(num_instancia[i]).strip('[]') + " port-priority 0 port-path-cost 1 port-spanning-tree-ind true encap-method llc lanfcs-ind forward  \n"
        tn.write(macbridge3.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1

    # Creación del GEM Port Network CTP vinculado al servicio multicast. Está asociado al puerto
    # 4094 (en el que va el servicio multicast). Se diferencia de la entidad que forma el servicio Ethernet
    # en que direction ya no es de tipo bidirectional sino de tipo ani-to-uni. Asimismo, esta entidad está
    # asociada a la entidad Multicast GEM Interworking Termination Point: el nº de instancia debe coincidir con
    # el campo gem-port-nwk-ctp-conn-ptr de la otra entidad.
    gemport_multicast = "gem-port-network-ctp create instance 2 port-id 4094 t-cont-ptr 0 direction ani-to-uni traffic-mgnt-ptr-ustream 0 traffic-descriptor-profile-ptr 0 priority-queue-ptr-downstream 0 traffic-descriptor-profile-ds-ptr 0 enc-key-ring 0  \n"
    tn.write(gemport_multicast.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Creación de los GEM Port Network CTP, que irán asociado a los puertos especificados 
    # anteriormente para los servicios Ethernet. Los identificadores tienen que coincidir 
    # con el gem-port-nwk-ct-conn-ptr de los GEM Interworking Termination Point.    
    i=0
    while i < num_servicios_Video:    
        gemport = "gem-port-network-ctp create instance " + str(num_instancia[i]).strip('[]') + " port-id  " + str(port_ID[i]).strip('[]') + "  t-cont-ptr " + str(puntero[i]).strip('[]') + " direction bidirectional traffic-mgnt-ptr-ustream 0 traffic-descriptor-profile-ptr 0 priority-queue-ptr-downstream 0 traffic-descriptor-profile-ds-ptr 0 enc-key-ring 0 \n"
        tn.write(gemport.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
     
    # Creación del Multicast GEM Interworking Termination Point (en este punto, se produce la 
    # transformación de flujo de bytes a tramas GEM y viceversa). Esta entidad está vinculada al
    # servicio multicast. Se vincula al GEM Port Netwok CTP de tipo multicast mediante el campo
    # gem-port-nwk-ctp-conn-ptr. También se asocia con elMAC Bridge Port Configuration Data de
    # tipo multicast. Para ello, hay que seleccionar como interwork-option mac-bridge y el campo 
    # service-profile-ptr debe tener el valor que se indica en la declaración del comando. 
    # El número de instancia debe ser el mismo que el tp-ptr del MAC Bridge Point Configuration Data asociado.
    multicast_geminterworking = "multicast-gem-interworking-termination-point create instance 2 gem-port-nwk-ctp-conn-ptr 2 interwork-option mac-bridge service-prof-ptr 65535 interwork-tp-ptr 0 gal-prof-ptr 65535 gal-lpbk-config 0 \n"
    tn.write(multicast_geminterworking.encode('ascii') + b"\n")
    time.sleep(0.2)
     
    # Creación de los GEM Interworking Termination Point (en este punto, se produce la 
    # transformación de flujo de bytes a tramas GEM y viceversa). Estas entidades se vinculan 
    # a los GEM Port Network CTP a través del campo gem-port-nwk-ctp-conn-ptr, que debe coincidir 
    # con el número de instancia que  utilizado en el GEM Port Network CTP. Estas entidades también 
    # se asocian con los MAC Bridge Port Configuration. Para ello, hay que seleccionar como 
    # interwork-option mac-bridge-lan y el campo service-profile-ptr debe ser un 1. 
    # El número de instancia debe ser el mismo que el tp-ptr del MAC Bridge Point Configuration Data.  
    i=0
    while i < num_servicios_Video:
        geminterworking = "gem-interworking-termination-point create instance " + str(num_instancia[i]).strip('[]') + " gem-port-nwk-ctp-conn-ptr " + str(num_instancia[i]).strip('[]') + " interwork-option mac-bridge-lan service-profile-ptr 1 interwork-tp-ptr 0 gal-profile-ptr 0 \n"
        tn.write(geminterworking.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # Creación de los VLAN Tagging Filter Data con los identficadores VLAN definidos arriba. 
    # Los números de instancia deben coincidir con los de los MAC Bridge Port Configuration Data asociados.
    i=0
    while i < num_servicios_Video:
        vlantagging = "vlan-tagging-filter-data create instance " + str(num_instancia[i]).strip('[]') + "  forward-operation h-vid-a vlan-tag1 " + str(VLAN_ID[i]).strip('[]') + " vlan-priority1 7  vlan-tag2 null vlan-priority2 null vlan-tag3 null vlan-priority3 null vlan-tag4 null vlan-priority4 null vlan-tag5 null vlan-priority5 null vlan-tag6 null vlan-priority6 null vlan-tag7 null vlan-priority7 null vlan-tag8 null vlan-priority8 null vlan-tag9 null vlan-priority9 null vlan-tag10 null vlan-priority10 null vlan-tag11 null vlan-priority11 null vlan-tag12 null vlan-priority12 null \n"
        tn.write(vlantagging.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1

    # Creación de la entidad Extended VLAN Tagging Operation Config Data, que será configurada en el paso posterior. 
    # Sirve para gestionar los identificadores VLAN. Esta entidad está asociada al primer MAC Bridge Port Configuration Data 
    # a través del número de instancia, que coincide con el tp-ptr del MAC Bridge Port Configuration Data.
    extendedvlan = "extended-vlan-tagging-operation-config-data create instance 257 association-type pptp-eth-uni associated-me-ptr 257 \n"
    tn.write(extendedvlan.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Configuración de la entidadExtended VLAN Tagging Operation Config Data. Se debe configurar
    # para cada identificador VLAN.
    i=0
    while i < num_servicios_Video:
        extendedvlanconf = "extended-vlan-tagging-operation-config-data set instance 257 operations-entry filter-outer-prio filter-prio-no-tag filter-outer-vid none filter-outer-tpid none filter-inner-prio filter-prio-none filter-inner-vid " + str(VLAN_ID[i]).strip('[]') + " filter-inner-tpid none filter-ethertype none treatment-tag-to-remove 1 treatment-outer-prio none treatment-outer-vid copy-from-inner treatment-outer-tpid tpid-de-copy-from-outer treatment-inner-prio 0 treatment-inner-vid " + str(VLAN_ID[i]).strip('[]') + " treatment-inner-tpid tpid-de-copy-from-inner\n"
        tn.write(extendedvlanconf.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # El comando exit hace salir hacia el menú anterior del CLI en la estructura de menús  
    salir = "exit \n"
    tn.write(salir.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Configuración de las reglas VLAN en la OLT asociadas a los puertos definidos al principio de la función.
    # Estos puertos son los vinculados a los servicios de tipo Ethernet.
    i=0
    while i < num_servicios_Video:
        reglasvlan = "vlan uplink configuration port-id " + str(port_ID[i]).strip('[]') + " min-cos 0 max-cos 7 de-bit disable primary-tag-handling false \n vlan uplink handling port-id  " + str(port_ID[i]).strip('[]') + "  primary-vlan none destination datapath c-vlan-handling no-change s-vlan-handling no-change new-c-vlan 0 new-s-vlan 0 \n"
        tn.write(reglasvlan.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # Creación de los perfiles de ancho de banda en sentido Downstream con los parámetros 
    # definidos por el usuario anteriormente. 
    i=0
    while i < num_servicios_Video:
        perfildownstreamconf = "policing downstream profile committed-max-bw " + str(BW_Downstream_GR[i]).strip('[]') + " committed-burst-size 1023 excess-max-bw " + str(BW_Downstream_Excess[i]).strip('[]') + " excess-burst-size 1023 \n"
        tn.write(perfildownstreamconf.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1
    
    # Nombre del fichero en el que se guardará la configuración del servicio de Internet + Video    
    nombre_archivo = 'Servicio_Internet+Video_ONU_MAC_' + MAC_ONU + '.txt'            
    # Al crear los perfiles de  ancho de banda en sentido de bajada, el CLI devuelve un 
    # índice de perfil. Para asociar los perfiles creados, se deben utilizar esos índices
    # de perfil y asociarlos a los puertos definidos anteriormente. Para ello, se vuelcan los datos
    # enviados y recibidos del CLI en un fichero, del que se extraerán esos índices.
    datos_perfil = tn.read_very_eager().decode()
    outfile = open(nombre_archivo, 'a')
    outfile.write(datos_perfil)
    outfile.close()
    outfile = open(nombre_archivo, 'r')             
    lines = outfile.readlines()
    true_ds_profile = 0
    # Se busca el índice de perfil que primero aparezca en el fichero.
    i=0
    for i in range (0,500):
        if true_ds_profile == 1:
            break                
        j = str(i)
        cadena = 'downstream_profile_index: ' + j + ' '
        for line in lines:
            if cadena in line:
                j = int(j)
                ds_profile_index[0] = j
                true_ds_profile = 1            
    
    outfile.close()
    
    # El índice encontrado corresponde al primer servicio. En caso de haber más, el índice
    # de perfil será ese aumentado en una unidad.
    i=0
    for i in range (0,num_servicios_Video):
        ds_profile_index[i] = ds_profile_index[0] + i
       
    # Asignación de los perfiles de ancho de banda Downstream a los puertos correspondientes
    # mediante los índices de perfil buscados anteriormente. Estos anchos de banda se asignan
    # a los servicios de tipo Ethernet. Para el servicio multicast, no hay que definir anchos
    # de banda.
    i=0
    while i < num_servicios_Video:
        perfildownstreamassign = "policing downstream port-configuration entity port-id " + str(port_ID[i]).strip('[]') + " ds-profile-index " + str(ds_profile_index[i]).strip('[]') + " \n"    
        tn.write(perfildownstreamassign.encode('ascii') + b"\n")    
        time.sleep(0.2)
        i=i+1
    
    # El comando exit hace salir hacia el menú anterior del CLI en la estructura de menús  
    salir = "exit \n"
    tn.write(salir.encode('ascii') + b"\n")
    time.sleep(0.2)

    # Desde el menú de configuración del algoritmo DBA, se definirián los perfiles de
    # ancho de banda en sentido Upstream
    dba = "pon \n dba pythagoras 0 \n "
    tn.write(dba.encode('ascii') + b"\n")
    time.sleep(0.2)
    
    # Cada perfil upstream irá asociado a un Alloc-ID y estará configurado con los
    # parámetros que haya definido el usuario. Como ya se se explicó anteriormente,
    # solo los servicios de tipo Ethernet tienen asociados Alloc-IDs; el de tipo 
    # multicast, no.
    i=0
    while i < num_servicios_Video:
        perfilupstream = "sla " + str(alloc_ID[i]).strip('[]') + " service data status-report nsr gr-bw " + str(BW_Upstream_GR[i]).strip('[]') + " gr-fine 0 be-bw " + str(BW_Upstream_BE[i]).strip('[]') + " be-fine 0 \n"     
        tn.write(perfilupstream.encode('ascii') + b"\n")
        time.sleep(0.2)
        i=i+1

    # El comando end hace salir directamente al modo privilegiado en la estructura
    # de menús del CLI
    final = "end \n"
    tn.write(final.encode('ascii') + b"\n")
    time.sleep(0.2)         
    
    # Se vuelcan todos los datos en el fichero definido anteriormente (la opción 'a'
    # hace que los datos se añadan al final del fichero) de forma que el fichero recogerá
    # toda la configuración del servicio de Internet + Vídeo
    data = tn.read_very_eager().decode() 
    outfile = open(nombre_archivo, 'a')
    outfile.write(data)
    outfile.write("\n\n\n")
    outfile.close()
    
    # Una vez configurado el servicio, se muestra un mensaje al usuario     
    print("Servicio de Internet+Vídeo configurado. \n")
    
    # Actualización del fichero XML con los datos configurados 
    doc = etree.parse("configuracionGPON.xml")
    # Extracción del elemento raíz
    redGPON = doc.getroot()
    
    # Obtención del índice correspondiente a la ONU que se está configurando.
    # Para ello, se hace una comparación de las diferentes direcciones MAC con 
    # la de la ONU a configurar.
    index=0
    true_index=0
    while true_index==0:
        for attr,value in redGPON[index].items():
            if value == MAC_ONU:
                true_index=1
                break
            index=index+1
    
    # Se borra cualquier configuración anterior que en este caso, al configurar un
    # nuevo servicio, se va a desechar.
    num_servicios = len(redGPON[index])
    i=0
    while i<num_servicios:
        redGPON[index].remove(redGPON[index][0])
        i=i+1
    
    # Se actualiza la parte del árbol correspondiente a la ONU en cuestión con los
    # parámetros que ha definido el usuario. Se utiliza la función SubElement para
    # crear el servicio así como los parámetros del mismo.                
    i=0
    while i < num_servicios_Video:       
        Servicio = etree.SubElement(redGPON[index], "Servicio", tipo='Internet+Video')
        VLAN = etree.SubElement(redGPON[index][i], "VLAN_ID").text = str(VLAN_ID[i]).strip('[]')
        BW_Down_GR = etree.SubElement(redGPON[index][i], "BW_Down_GR").text = str(BW_Downstream_GR[i]).strip('[]')                        
        BW_Down_Excess = etree.SubElement(redGPON[index][i], "BW_Down_Excess").text = str(BW_Downstream_Excess[i]).strip('[]')
        BW_Up_GR = etree.SubElement(redGPON[index][i], "BW_Up_GR").text = str(BW_Upstream_GR[i]).strip('[]')                        
        BW_Up_BE = etree.SubElement(redGPON[index][i], "BW_Up_BE").text = str(BW_Upstream_BE[i]).strip('[]')   
        i=i+1                            
    
    # Finalmente se crea el nuevo árbol modificado y se escribe en el fichero XML
    doc = etree.ElementTree(redGPON)
    doc.write("configuracionGPON.xml")
                  
    return

# Función modificar_configuracion(ID_ONU,MAC_ONU,nombre_archivo): esta función permite
# cambiar la configuración existente en una ONU. Toma como argumento el identificador
# de ONU, la dirección MAC y el nombre del archivo donde está la configuración que será
# reemplazada. La función en primer lugar borra la configuración existente tomando datos de este
# fichero y posteriormente, llama a las funciones de servicio_Internet o servicio_Video
# dependiendo de lo que el usuario haya seleccionado para la nueva configuración.   
def modificar_configuracion(ID_ONU,MAC_ONU,nombre_archivo):
        
    # Host y puerto al que se hace la conexión Telnet para acceder al CLI                    
    host = "172.26.128.38"
    port = "4551"
     
    # Claves de acceso al CLI
    password1 = "TLNT25"
    password2 = "TLNT145"
    enable = "enable"
    
    # Acceso al CLI: conexión Telnet al host y puerto indicados anteriormente
    tn = telnetlib.Telnet(host,port,1)
    # Mediante la función write de telnetlib, escritura de los comandos que permiten
    # acceder al menú de privilegios del CLI
    tn.write(password1.encode('ascii') + b"\n")      
    time.sleep(0.1)     
    tn.write(enable.encode('ascii') + b"\n")
    time.sleep(0.1)     
    tn.write(password2.encode('ascii') + b"\n")
    time.sleep(0.1) 
    
    # Se abre en modo lectura el fichero en el que está la configuración antigua   
    outfile = open(nombre_archivo, 'r')             
    # Se almacenan todas las líneas del fichero
    lines = outfile.readlines()
    port_ID = 0
    
    # En este bucle, se buscarán los puertos configurados y se borrarán los perfiles
    # de anchos de banda asociados a estos puertos. En las funciones de servicio_Internet
    # y servicio_Video que se llamarán posteriormente, se borrarán las entidades MIB, de forma
    # que los servicios quedarán borrados por completo.    
    k=0
    for k in range (0,10000):              
        j = str(k)
        # Se recorren todas las líneas del fichero hasta encontrar los puertos configurados
        cadena = 'configuration entity port-id ' + j + ' '
        for line in lines:
            if cadena in line:
                j = int(j)
                port_ID = j
                port_ID = str(port_ID)
                # Cada vez que se encuentra un puerto configurado, se borra el perfil de ancho
                # de banda asociado
                borrar_perfil = "configure \n olt-device 0 \n olt-channel 0 \n no policing downstream port-configuration entity port-id " + port_ID + " \n end \n"
                tn.write(borrar_perfil.encode('ascii') + b"\n")
                time.sleep(0.1)
    
    outfile.close()    
    # Finalmente se borra el fichero de configuración
    os.remove(nombre_archivo)
    
    # Actualización del archivo XML con los datos borrados (se actualizarán en las 
    # funciones correspondientes de Internet o Vídeo)
    doc = etree.parse("configuracionGPON.xml")
    # Extracción del elemento raíz
    redGPON = doc.getroot()
    
    # Obtención del índice correspondiente a la ONU cuya configuración se va a borrar.
    # Para ello, se hace una comparación de las diferentes direcciones MAC con 
    # la de la ONU en cuestión.
    index=0
    true_index=0
    while true_index==0:
        for attr,value in redGPON[index].items():
            if value == MAC_ONU:
                true_index=1
                break
            index=index+1
    
    # Se borra la configuración de dicha ONU
    num_servicios = len(redGPON[index])
    i=0
    while i<num_servicios:
        redGPON[index].remove(redGPON[index][0])
        i=i+1
    
    # Finalmente se crea el nuevo árbol modificado y se escribe en el fichero XML
    doc = etree.ElementTree(redGPON)
    doc.write("configuracionGPON.xml")

    # Una vez borrada la configuración, se pide al usuario que seleccione la nueva
    # configuración que desea para la ONU. Dos opciones: servicio de Internet y 
    # servicio de Internet + Video. 
    true_opcion = 0
    while true_opcion == 0:
        print("\n¿Qué servicio quiere crear ahora en esta ONU? Escoja la opción:")
        print("1 - Servicio de Internet")
        print("2 - Servicio de Internet + Vídeo")
        opcion = input()
        opcion = int(opcion)
        # Si el usuario no selecciona una opción válida, se le vuelve a pedir.
        if opcion == 1 or opcion == 2:
            true_opcion = 1
        else:
            print("Opción no válida. Vuelva a probar.")        
    
    # En caso de seleccionar el servicio de Internet, se le pide el nº de servicios.
    # Este nº podría ser mayor que 2 pero realmente, no tiene sentido configurar más de 2 
    # servicios puesto que solo hay 2 VLAN que proporcionen servicio a esta red. 
    if opcion == 1:
        print("\nConfiguración del servicio de Internet:")
        cont=0
        true_Internet = 0
        while true_Internet == 0:
            if cont == 0:
                print("Número de servicios de datos:")
            else:
                print("Número de servicios no válido. Vuelva a probar:")
            cont = cont+1
            num_servicios_Internet = input()
            num_servicios_Internet = int(num_servicios_Internet)
            if num_servicios_Internet > -1 and num_servicios_Internet < 3:
                true_Internet = 1
        
        print("\n")
        # En caso de que el usuario seleccione un número de servicios de Internet que no sea 0,
        # se llama a la función de servicio_Internet para configurar la ONU con los nuevos parámetros
        if num_servicios_Internet > 0: 
            servicio_Internet(ID_ONU,MAC_ONU,num_servicios_Internet)

    # En caso de seleccionar el servicio de Internet+Video, se le pide el nº de servicios.
    # Este nº podría ser mayor que 2 pero realmente, no tiene sentido configurar más de 2 
    # servicios puesto que solo hay 2 VLAN que proporcionen servicio a esta red.
    elif opcion == 2:
        print("\nConfiguración del servicio de Internet + Vídeo:")
        cont=0
        true_Video = 0
        while true_Video == 0:
            if cont == 0:
                print("Número de servicios de vídeo:")
            else:
                print("Número de servicios no válido. Vuelva a probar:")
            cont = cont+1
            num_servicios_Video = input()
            num_servicios_Video = int(num_servicios_Video)
            if num_servicios_Video > -1 and num_servicios_Video < 3:
                true_Video = 1
        
        print("\n")
        # En caso de que el usuario seleccione un número de servicios de Internet + Video que no sea 0,
        # se llama a la función de servicio_Internet para configurar la ONU con los nuevos parámetros
        if num_servicios_Video > 0: 
            servicio_Video(ID_ONU,MAC_ONU,num_servicios_Video)
          
    return
 
# Función borrar_configuracion(ID_ONU,MAC_ONU,nombre_archivo): esta función permite
# borrar la configuración de una ONU. Toma como argumento el identificador
# de ONU, la dirección MAC y el nombre del archivo donde está la configuración que 
# será borrada. 
def borrar_configuracion(ID_ONU,MAC_ONU,nombre_archivo):

    # Host y puerto al que se hace la conexión Telnet para acceder al CLI                    
    host = "172.26.128.38"
    port = "4551"
     
    # Claves de acceso al CLI
    password1 = "TLNT25"
    password2 = "TLNT145"
    enable = "enable"
    
    # Acceso al CLI: conexión Telnet al host y puerto indicados anteriormente
    tn = telnetlib.Telnet(host,port,1)
    # Mediante la función write de telnetlib, escritura de los comandos que permiten
    # acceder al menú de privilegios del CLI
    tn.write(password1.encode('ascii') + b"\n")      
    time.sleep(0.1)     
    tn.write(enable.encode('ascii') + b"\n")
    time.sleep(0.1)     
    tn.write(password2.encode('ascii') + b"\n")
    time.sleep(0.1) 
    
    # Se abre en modo lectura el fichero en el que está la configuración a borrar.   
    outfile = open(nombre_archivo, 'r')             
    # Se almacenan todas las líneas del fichero.
    lines = outfile.readlines()
    port_ID = 0
    
    # En este bucle, se buscarán los puertos configurados y se borrarán los perfiles
    # de anchos de banda asociados a estos puertos. Posteriormente, se borrarán las entidades
    # que forman los servicios.
    k=0
    for k in range (0,10000):              
        j = str(k)
        # Se recorren todas las líneas del fichero hasta encontrar los puertos configurados.
        cadena = 'configuration entity port-id ' + j + ' '
        for line in lines:
            if cadena in line:
                j = int(j)
                port_ID = j
                port_ID = str(port_ID)
                # Cada vez que se encuentra un puerto configurado, se borra el perfil de ancho
                # de banda asociado.
                borrar_perfil = "configure \n olt-device 0 \n olt-channel 0 \n no policing downstream port-configuration entity port-id " + port_ID + " \n end \n"
                tn.write(borrar_perfil.encode('ascii') + b"\n")
                time.sleep(0.1)
    
    outfile.close()    
    ID_ONU = str(ID_ONU)
    # Tras borrar los perfiles de ancho de banda, se borrar las entidades MIB presentes en 
    # el canal OMCI asociado a la ONU.
    borrar_MIB = "configure \n olt-device 0 \n olt-channel 0 \n onu-local " + ID_ONU + " \n omci-port  " + ID_ONU + "  \n exit \n onu-omci  " + ID_ONU + " \n ont-data mib-reset \n exit \n end \n"
    tn.write(borrar_MIB.encode('ascii') + b"\n")
    time.sleep(2)
    # Finalmente se borra el fichero de configuración.
    os.remove(nombre_archivo)
    # Una vez borrado, se muestra un mensaje informando al usuario.
    print("\nLa configuración de la ONU con número de serie " + MAC_ONU + " ha sido borrada.\n")
    
   # Actualización del archivo XML con los datos borrados
    doc = etree.parse("configuracionGPON.xml")
    # Extracción del elemento raíz
    redGPON = doc.getroot()
    
    # Obtención del índice correspondiente a la ONU cuya configuración se va a borrar.
    # Para ello, se hace una comparación de las diferentes direcciones MAC con 
    # la de la ONU en cuestión.
    index=0
    true_index=0
    while true_index==0:
        for attr,value in redGPON[index].items():
            if value == MAC_ONU:
                true_index=1
                break
            index=index+1
    
    # Se borra la configuración de dicha ONU
    num_servicios = len(redGPON[index])
    i=0
    while i<num_servicios:
        redGPON[index].remove(redGPON[index][0])
        i=i+1
    
    # Finalmente se crea el nuevo árbol modificado y se escribe en el fichero XML
    doc = etree.ElementTree(redGPON)
    doc.write("configuracionGPON.xml")
    
    return

# Función ver_configuracion(ID_ONU,MAC_ONU,nombre_archivo): esta función permite
# ver la configuración presente en una ONU. Toma como argumento el identificador
# de ONU, la dirección MAC y el nombre del archivo donde está la configuración.
# Para mostrar esta configuración, se utiliza el fichero XML que se va actualizando
# con las llamadas a cada función.
def ver_configuracion(MAC_ONU):

    # Extracción del elemento raíz del fichero XML.
    doc = etree.parse("configuracionGPON.xml")
    redGPON = doc.getroot()   

    # Obtención del índice correspondiente a la ONU cuya configuración se va a mostrar.
    # Para ello, se hace una comparación de las diferentes direcciones MAC con 
    # la de la ONU en cuestión.
    index=0
    true_index=0
    while true_index == 0:
        for attr,value in redGPON[index].items():
            if value == MAC_ONU:
                true_index=1
                break
            index = index+1
    
    # Obtención del tipo de servicio: Internet o Internet + Video.
    # Para ello, se obtiene el valor que toma el subelemento Servicio.       
    for attr,value in redGPON[index][0].items():
        if value == 'Internet':
            tipo_servicio = value
        elif value == 'Internet+Video':
            tipo_servicio = value
    
    # Obtención del nº de servicios que había configurados en base a la longitud
    # del vector de subelementos de la ONU.    
    num_servicios = len(redGPON[index]) 
    print("\n")
    # Se muestra el tipo de servicio, el número de servicios y los parámetros que los forman.
    print("Tipo de servicio: " + tipo_servicio)
    print("Número de servicios:", num_servicios)
    print("Parámetros:\n")
    
    # Para mostrar los parámetros, se accede a los subelementos de los servicios.
    i=0
    while i<num_servicios:
        print("El identificador VLAN del servicio", i+1,"es " + redGPON[index][i][0].text)
        print("El ancho de banda garantizado Downstream en Kbps del servicio", i+1, "es " + redGPON[index][i][1].text)
        print("El ancho de banda en exceso Downstream en Kbps del servicio", i+1, "es " + redGPON[index][i][2].text)
        print("El ancho de banda garantizado Upstream en Mbps del servicio", i+1, "es " + redGPON[index][i][3].text)
        print("El ancho de banda Best Effort Upstream en Mbps del servicio", i+1, "es " + redGPON[index][i][4].text)
        print("\n")
        i=i+1
    
    return

# Función cargar_configuracion(ID_ONU,MAC_ONU): esta función permite recuperar y
# cargar la configuración que había en una ONU antes de que la red fuera apagada o
# se cambiara al modo de gestión TGMS. Con esta función, se solventa el problema de la
# no persistencia del CLI. Toma como parámetros el ID de la ONU y la dirección MAC. Utiliza
# los datos guardados en el fichero XML para cargar la configuración que se había perdido.
def cargar_configuracion(ID_ONU,MAC_ONU):

    # Extracción del elemento raíz del fichero XML.
    doc = etree.parse("configuracionGPON.xml")
    redGPON = doc.getroot()

    # Obtención del índice correspondiente a la ONU cuya configuración se va a cargar.
    # Para ello, se hace una comparación de las diferentes direcciones MAC con 
    # la de la ONU en cuestión.
    index=0
    true_index=0
    while true_index == 0:
        for attr,value in redGPON[index].items():
            if value == MAC_ONU:
                true_index=1
                break
            index = index+1
    
    # Obtención del nº de servicios que había configurados en base a la longitud
    # del vector de subelementos de la ONU.
    num_servicios = len(redGPON[index])   
    
    # Este if obtiene el tipo de servicio (si es que había alguno) configurado.
    # Para ello, se obtiene el valor que toma el subelemento Servicio.        
    if num_servicios>0:
        for attr,value in redGPON[index][0].items():
            if value == 'Internet':
                tipo_servicio = value
            elif value == 'Internet+Video':
                tipo_servicio = value
    elif num_servicios == 0:
        # En caso de que no hubiera servicios configurados, se informa con un mensaje
        # y se retorna al main del programa.
        print("\nLa ONU con número de serie " + MAC_ONU + " no estaba configurada.\n")
        return           
    
    # Si el servicio configurado anteriormente era de Internet, se muestra un mensaje informando de ello.
    if tipo_servicio == 'Internet':
        print("\nLa ONU con número de serie " + MAC_ONU + " tenía configurado anteriormente servicio de Internet.")
        
        # A partir de aquí se reproduce la función servicio_Internet con la DIFERENCIA 
        # de que los parámetros ya no se piden al usuario, sino que se toman del 
        # del fichero XML.
        
        # ¡IMPORTANTE! Las líneas que siguen son iguales que las de la función servicio_Internet
        # PARA LA EXPLICACIÓN DE LAS SIGUIENTES LÍNEAS, VER FUNCIÓN servicio_Internet
        port_ID = []
        alloc_ID = [] 
        tcont_ID = []
        num_instancia = []
        puntero = []
        ds_profile_index = []
        i=0    
        while i < num_servicios: 
            port_ID.append(200+100*ID_ONU+i)
            alloc_ID.append(200+100*ID_ONU+i)        
            num_instancia.append(i+2)
            tcont_ID.append(i)
            puntero.append(32768+i)
            ds_profile_index.append(i)
            i=i+1 
           
        print("\n")
                    
        host = "172.26.128.38"
        port = "4551"             
        password1 = "TLNT25"
        password2 = "TLNT145"
        enable = "enable"    
        
        tn = telnetlib.Telnet(host,port,1)
        tn.write(password1.encode('ascii') + b"\n")      
        time.sleep(0.2)
        tn.write(enable.encode('ascii') + b"\n")
        time.sleep(0.2)     
        tn.write(password2.encode('ascii') + b"\n")
        time.sleep(0.2)
          
        ID_ONU = str(ID_ONU)
        inicio = "configure \n olt-device 0 \n olt-channel 0 \n onu-local " + ID_ONU + " \n omci-port  " + ID_ONU + "  \n exit \n onu-omci  " + ID_ONU + "  \n ont-data mib-reset \n exit \n fec direction uplink  " + ID_ONU + "  \n onu-local  " + ID_ONU + "  \n"                
        tn.write(inicio.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios: 
            allocID = "alloc-id " + str(alloc_ID[i]).strip('[]') + " \n"
            tn.write(allocID.encode('ascii') + b"\n")
            time.sleep(0.2) 
            i=i+1
            
        salir = "exit \n"
        tn.write(salir.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            portalloc = "port " + str(port_ID[i]).strip('[]') + " alloc-id  " + str(alloc_ID[i]).strip('[]') + " \n"
            tn.write(portalloc.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
        
        omci = "onu-omci " + ID_ONU + " \n"
        tn.write(omci.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            tcont = "t-cont set slot-id 128 t-cont-id " + str(tcont_ID[i]).strip('[]') + " alloc-id " + str(alloc_ID[i]).strip('[]') + "  \n"
            tn.write(tcont.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1       
        
        macservice = "mac-bridge-service-profile create slot-id 0 bridge-group-id 1 spanning-tree-ind true learning-ind true atm-port-bridging-ind true priority 32000 max-age 1536 hello-time 256 forward-delay 1024 unknown-mac-address-discard false mac-learning-depth 255 dynamic-filtering-ageing-time 1000 \n"
        macbridge1 = "mac-bridge-pcd create instance 1 bridge-id-ptr 1 port-num 1 tp-type lan tp-ptr 257 port-priority 2 port-path-cost 32 port-spanning-tree-ind true encap-method llc lanfcs-ind forward \n"
        tn.write(macservice.encode('ascii') + b"\n")
        time.sleep(0.2)
        tn.write(macbridge1.encode('ascii') + b"\n")
        time.sleep(0.2) 
        
        i=0
        while i < num_servicios:   
            macbridge2 = "mac-bridge-pcd create instance " + str(num_instancia[i]).strip('[]') + " bridge-id-ptr 1 port-num " + str(num_instancia[i]).strip('[]') + " tp-type gem tp-ptr " + str(num_instancia[i]).strip('[]') + " port-priority 0 port-path-cost 1 port-spanning-tree-ind true encap-method llc lanfcs-ind forward  \n"
            tn.write(macbridge2.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
         
        i=0
        while i < num_servicios:    
            gemport = "gem-port-network-ctp create instance " + str(num_instancia[i]).strip('[]') + " port-id  " + str(port_ID[i]).strip('[]') + "  t-cont-ptr " + str(puntero[i]).strip('[]') + " direction bidirectional traffic-mgnt-ptr-ustream 0 traffic-descriptor-profile-ptr 0 priority-queue-ptr-downstream 0 traffic-descriptor-profile-ds-ptr 0 enc-key-ring 0 \n"
            tn.write(gemport.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
            
        i=0
        while i < num_servicios:
            geminterworking = "gem-interworking-termination-point create instance " + str(num_instancia[i]).strip('[]') + " gem-port-nwk-ctp-conn-ptr " + str(num_instancia[i]).strip('[]') + " interwork-option mac-bridge-lan service-profile-ptr 1 interwork-tp-ptr 0 gal-profile-ptr 0 \n"
            tn.write(geminterworking.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
    
        i=0
        while i < num_servicios:
            vlantagging = "vlan-tagging-filter-data create instance " + str(num_instancia[i]).strip('[]') + " forward-operation h-vid-a vlan-tag1 " + redGPON[index][i][0].text + " vlan-priority1 7 vlan-tag2 null vlan-priority2 null vlan-tag3 null vlan-priority3 null vlan-tag4 null vlan-priority4 null vlan-tag5 null vlan-priority5 null vlan-tag6 null vlan-priority6 null vlan-tag7 null vlan-priority7 null vlan-tag8 null vlan-priority8 null vlan-tag9 null vlan-priority9 null vlan-tag10 null vlan-priority10 null vlan-tag11 null vlan-priority11 null vlan-tag12 null vlan-priority12 null \n"
            tn.write(vlantagging.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
    
        extendedvlan = "extended-vlan-tagging-operation-config-data create instance 257 association-type pptp-eth-uni associated-me-ptr 257 \n"
        tn.write(extendedvlan.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            extendedvlanconf = "extended-vlan-tagging-operation-config-data set instance 257 operations-entry filter-outer-prio filter-prio-no-tag filter-outer-vid none filter-outer-tpid none filter-inner-prio filter-prio-none filter-inner-vid " + redGPON[index][i][0].text + " filter-inner-tpid none filter-ethertype none treatment-tag-to-remove 1 treatment-outer-prio none treatment-outer-vid copy-from-inner treatment-outer-tpid tpid-de-copy-from-outer treatment-inner-prio 0 treatment-inner-vid " + redGPON[index][i][0].text + " treatment-inner-tpid tpid-de-copy-from-inner\n"
            tn.write(extendedvlanconf.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
        
        salir = "exit \n"
        tn.write(salir.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            reglasvlan = "vlan uplink configuration port-id " + str(port_ID[i]).strip('[]') + " min-cos 0 max-cos 7 de-bit disable primary-tag-handling false \n vlan uplink handling port-id  " + str(port_ID[i]).strip('[]') + "  primary-vlan none destination datapath c-vlan-handling no-change s-vlan-handling no-change new-c-vlan 0 new-s-vlan 0 \n"
            tn.write(reglasvlan.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
        
        i=0
        while i < num_servicios:
            perfildownstreamconf = "policing downstream profile committed-max-bw " + redGPON[index][i][1].text + " committed-burst-size 1023 excess-max-bw " + redGPON[index][i][2].text + " excess-burst-size 1023 \n"
            tn.write(perfildownstreamconf.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
          
        nombre_archivo = 'Servicio_Internet_ONU_MAC_' + MAC_ONU + '.txt'    
        datos_perfil = tn.read_very_eager().decode()
        outfile = open(nombre_archivo, 'a')
        outfile.write(datos_perfil)
        outfile.close()
        outfile = open(nombre_archivo, 'r')             
        lines = outfile.readlines()
        true_ds_profile = 0
        i=0
        for i in range (0,500):
            if true_ds_profile == 1:
                break                
            j = str(i)
            cadena = 'downstream_profile_index: ' + j + ' '
            for line in lines:
                if cadena in line:
                    j = int(j)
                    ds_profile_index[0] = j
                    true_ds_profile = 1            
        
        outfile.close()
        
        i=0
        for i in range (0,num_servicios):
            ds_profile_index[i] = ds_profile_index[0] + i
                
        i=0
        while i < num_servicios:
            perfildownstreamassign = "policing downstream port-configuration entity port-id " + str(port_ID[i]).strip('[]') + " ds-profile-index " + str(ds_profile_index[i]).strip('[]') + " \n"    
            tn.write(perfildownstreamassign.encode('ascii') + b"\n")    
            time.sleep(0.2)
            i=i+1
            
        salir = "exit \n"
        tn.write(salir.encode('ascii') + b"\n")
        time.sleep(0.2)
    
        dba = "pon \n dba pythagoras 0 \n "
        tn.write(dba.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            perfilupstream = "sla " + str(alloc_ID[i]).strip('[]') + " service data status-report nsr gr-bw " + redGPON[index][i][3].text + " gr-fine 0 be-bw " + redGPON[index][i][4].text + " be-fine 0 \n"     
            tn.write(perfilupstream.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
    
        final = "end \n"
        tn.write(final.encode('ascii') + b"\n")
        time.sleep(0.2)         
        
        data = tn.read_very_eager().decode() 
        outfile = open(nombre_archivo, 'a')
        outfile.write(data)
        outfile.write("\n\n\n")
        outfile.close()  
        
        # Aquí finaliza la configuración del servicio de Internet con los datos tomados
        # del fichero XML para recuperar la configuración anterior. Una vez estabecida esta
        # nueva configuración, se ha vuelto a crear el fichero txt con los datos del CLI para
        # su utilización en otras funciones. Es importante recordar que estos ficheros txt deben
        #ser borados manualmente cada vez que se apaga la red o se cambia al TGMS. 
        
        # Se muestra un mensaje al usuario informando que la configuración se ha restablecido
        # correctamente y se le pregunta si desea ver dicha configuración.
        print("El servicio de Internet configurado anteriormente ha sido restablecido.\n")
        print("¿Desea ver la configuración cargada? Responda SI o NO") 
        true_respuesta=0
        while true_respuesta == 0:
            respuesta = input()
            # En caso de que quiera ver la configuración, se llama a la función ver_configuración
            if respuesta == "SI":
                ver_configuracion(MAC_ONU)
                true_respuesta = 1
            elif respuesta == "NO":
                print("\n")
                true_respuesta = 1
            else:
                print("Vuelva a introducir su respuesta (SI o NO)")             

    # Si el servicio configurado anteriormente era de Internet + Video, se muestra un mensaje informando de ello.    
    elif tipo_servicio == 'Internet+Video': 
        print("\nLa ONU con número de serie " + MAC_ONU + " tenía configurado anteriormente servicio de Internet y Video.")
        
        # A partir de aquí se reproduce la función servicio_Video con la DIFERENCIA 
        # de que los parámetros ya no se piden al usuario, sino que se toman del 
        # del fichero XML.
        
        # ¡IMPORTANTE! Las líneas que siguen son iguales que las de la función servicio_Video
        # PARA LA EXPLICACIÓN DE LAS SIGUIENTES LÍNEAS, VER FUNCIÓN servicio_Video
        port_ID = []
        alloc_ID = [] 
        tcont_ID = []
        num_instancia = []
        puntero = []
        ds_profile_index = []
        i=0
    
        while i < num_servicios:
            port_ID.append(200+100*ID_ONU+i)
            alloc_ID.append(200+100*ID_ONU+i)          
            num_instancia.append(i+3)
            tcont_ID.append(i)
            puntero.append(32768+i)
            ds_profile_index.append(i)
            i=i+1 

        host = "172.26.128.38"
        port = "4551"             
        password1 = "TLNT25"
        password2 = "TLNT145"
        enable = "enable"    
        
        tn = telnetlib.Telnet(host,port,1)
        tn.write(password1.encode('ascii') + b"\n")      
        time.sleep(0.2)
        tn.write(enable.encode('ascii') + b"\n")
        time.sleep(0.2)     
        tn.write(password2.encode('ascii') + b"\n")
        time.sleep(0.2)
          
        ID_ONU = str(ID_ONU)
        inicio = "configure \n olt-device 0 \n olt-channel 0 \n onu-local " + ID_ONU + " \n omci-port  " + ID_ONU + "  \n exit \n onu-omci  " + ID_ONU + "  \n ont-data mib-reset \n exit \n fec direction uplink  " + ID_ONU + "  \n onu-local  " + ID_ONU + "  \n"                
        tn.write(inicio.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios: 
            allocID = "alloc-id " + str(alloc_ID[i]).strip('[]') + " \n"
            tn.write(allocID.encode('ascii') + b"\n")
            time.sleep(0.2) 
            i=i+1
            
        salir = "exit \n"
        tn.write(salir.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            portalloc = "port " + str(port_ID[i]).strip('[]') + " alloc-id  " + str(alloc_ID[i]).strip('[]') + " \n"
            tn.write(portalloc.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
        
        omci = "onu-omci " + ID_ONU + " \n"
        tn.write(omci.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            tcont = "t-cont set slot-id 128 t-cont-id " + str(tcont_ID[i]).strip('[]') + " alloc-id " + str(alloc_ID[i]).strip('[]') + "  \n"
            tn.write(tcont.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1       
        
        macservice = "mac-bridge-service-profile create slot-id 0 bridge-group-id 1 spanning-tree-ind true learning-ind true atm-port-bridging-ind true priority 32000 max-age 1536 hello-time 256 forward-delay 1024 unknown-mac-address-discard false mac-learning-depth 255 dynamic-filtering-ageing-time 1000 \n"
        macbridge1 = "mac-bridge-pcd create instance 1 bridge-id-ptr 1 port-num 1 tp-type lan tp-ptr 257 port-priority 2 port-path-cost 32 port-spanning-tree-ind true encap-method llc lanfcs-ind forward \n"
        tn.write(macservice.encode('ascii') + b"\n")
        time.sleep(0.2)
        tn.write(macbridge1.encode('ascii') + b"\n")
        time.sleep(0.2) 
        
        macbridge2 = "mac-bridge-pcd create instance 2 bridge-id-ptr 1 port-num 2 tp-type mc-gem tp-ptr 2 port-priority 0 port-path-cost 1 port-spanning-tree-ind true encap-method llc lanfcs-ind forward  \n"
        tn.write(macbridge2.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:   
            macbridge3 = "mac-bridge-pcd create instance " + str(num_instancia[i]).strip('[]') + " bridge-id-ptr 1 port-num " + str(num_instancia[i]).strip('[]') + " tp-type gem tp-ptr " + str(num_instancia[i]).strip('[]') + " port-priority 0 port-path-cost 1 port-spanning-tree-ind true encap-method llc lanfcs-ind forward  \n"
            tn.write(macbridge3.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
    
        gemport_multicast = "gem-port-network-ctp create instance 2 port-id 4094 t-cont-ptr 0 direction ani-to-uni traffic-mgnt-ptr-ustream 0 traffic-descriptor-profile-ptr 0 priority-queue-ptr-downstream 0 traffic-descriptor-profile-ds-ptr 0 enc-key-ring 0  \n"
        tn.write(gemport_multicast.encode('ascii') + b"\n")
        time.sleep(0.2)
            
        i=0
        while i < num_servicios:    
            gemport = "gem-port-network-ctp create instance " + str(num_instancia[i]).strip('[]') + " port-id  " + str(port_ID[i]).strip('[]') + "  t-cont-ptr " + str(puntero[i]).strip('[]') + " direction bidirectional traffic-mgnt-ptr-ustream 0 traffic-descriptor-profile-ptr 0 priority-queue-ptr-downstream 0 traffic-descriptor-profile-ds-ptr 0 enc-key-ring 0 \n"
            tn.write(gemport.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
            
        multicast_geminterworking = "multicast-gem-interworking-termination-point create instance 2 gem-port-nwk-ctp-conn-ptr 2 interwork-option mac-bridge service-prof-ptr 65535 interwork-tp-ptr 0 gal-prof-ptr 65535 gal-lpbk-config 0 \n"
        tn.write(multicast_geminterworking.encode('ascii') + b"\n")
        time.sleep(0.2)
            
        i=0
        while i < num_servicios:
            geminterworking = "gem-interworking-termination-point create instance " + str(num_instancia[i]).strip('[]') + " gem-port-nwk-ctp-conn-ptr " + str(num_instancia[i]).strip('[]') + " interwork-option mac-bridge-lan service-profile-ptr 1 interwork-tp-ptr 0 gal-profile-ptr 0 \n"
            tn.write(geminterworking.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
    
        i=0
        while i < num_servicios:
            vlantagging = "vlan-tagging-filter-data create instance " + str(num_instancia[i]).strip('[]') + "  forward-operation h-vid-a vlan-tag1 " + redGPON[index][i][0].text + " vlan-priority1 7  vlan-tag2 null vlan-priority2 null vlan-tag3 null vlan-priority3 null vlan-tag4 null vlan-priority4 null vlan-tag5 null vlan-priority5 null vlan-tag6 null vlan-priority6 null vlan-tag7 null vlan-priority7 null vlan-tag8 null vlan-priority8 null vlan-tag9 null vlan-priority9 null vlan-tag10 null vlan-priority10 null vlan-tag11 null vlan-priority11 null vlan-tag12 null vlan-priority12 null \n"
            tn.write(vlantagging.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
    
        extendedvlan = "extended-vlan-tagging-operation-config-data create instance 257 association-type pptp-eth-uni associated-me-ptr 257 \n"
        tn.write(extendedvlan.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            extendedvlanconf = "extended-vlan-tagging-operation-config-data set instance 257 operations-entry filter-outer-prio filter-prio-no-tag filter-outer-vid none filter-outer-tpid none filter-inner-prio filter-prio-none filter-inner-vid " + redGPON[index][i][0].text + " filter-inner-tpid none filter-ethertype none treatment-tag-to-remove 1 treatment-outer-prio none treatment-outer-vid copy-from-inner treatment-outer-tpid tpid-de-copy-from-outer treatment-inner-prio 0 treatment-inner-vid " + redGPON[index][i][0].text + " treatment-inner-tpid tpid-de-copy-from-inner\n"
            tn.write(extendedvlanconf.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
        
        salir = "exit \n"
        tn.write(salir.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            reglasvlan = "vlan uplink configuration port-id " + str(port_ID[i]).strip('[]') + " min-cos 0 max-cos 7 de-bit disable primary-tag-handling false \n vlan uplink handling port-id " + str(port_ID[i]).strip('[]') + " primary-vlan none destination datapath c-vlan-handling no-change s-vlan-handling no-change new-c-vlan 0 new-s-vlan 0 \n"
            tn.write(reglasvlan.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
        
        i=0
        while i < num_servicios:
            perfildownstreamconf = "policing downstream profile committed-max-bw " + redGPON[index][i][1].text + " committed-burst-size 1023 excess-max-bw " + redGPON[index][i][2].text + " excess-burst-size 1023 \n"
            tn.write(perfildownstreamconf.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
        
        nombre_archivo = 'Servicio_Internet+Video_ONU_MAC_' + MAC_ONU + '.txt'    
        datos_perfil = tn.read_very_eager().decode()
        outfile = open(nombre_archivo, 'a')
        outfile.write(datos_perfil)
        outfile.close()
        outfile = open(nombre_archivo, 'r')             
        lines = outfile.readlines()
        true_ds_profile = 0
        i=0
        for i in range (0,100):
            if true_ds_profile == 1:
                break                
            j = str(i)
            cadena = 'downstream_profile_index: ' + j + ' '
            for line in lines:
                if cadena in line:
                    j = int(j)
                    ds_profile_index[0] = j
                    true_ds_profile = 1        
        outfile.close()
        
        i=0
        for i in range (0,num_servicios):
            ds_profile_index[i] = ds_profile_index[0] + i
           
     
        i=0
        while i < num_servicios:
            perfildownstreamassign = "policing downstream port-configuration entity port-id " + str(port_ID[i]).strip('[]') + " ds-profile-index " + str(ds_profile_index[i]).strip('[]') + " \n"    
            tn.write(perfildownstreamassign.encode('ascii') + b"\n")    
            time.sleep(0.2)
            i=i+1
            
        salir = "exit \n"
        tn.write(salir.encode('ascii') + b"\n")
        time.sleep(0.2)
    
        dba = "pon \n dba pythagoras 0 \n "
        tn.write(dba.encode('ascii') + b"\n")
        time.sleep(0.2)
        
        i=0
        while i < num_servicios:
            perfilupstream = "sla " + str(alloc_ID[i]).strip('[]') + "  service data status-report nsr gr-bw " + redGPON[index][i][3].text + " gr-fine 0 be-bw " + redGPON[index][i][4].text + " be-fine 0 \n"     
            tn.write(perfilupstream.encode('ascii') + b"\n")
            time.sleep(0.2)
            i=i+1
    
        final = "end \n"
        tn.write(final.encode('ascii') + b"\n")
        time.sleep(0.2)         
        
        data = tn.read_very_eager().decode() 
        outfile = open(nombre_archivo, 'a')
        outfile.write(data)
        outfile.write("\n\n\n")
        outfile.close()

        # Aquí finaliza la configuración del servicio de Internet + Video con los datos tomados
        # del fichero XML para recuperar la configuración anterior. Una vez estabecida esta
        # nueva configuración, se ha vuelto a crear el fichero txt con los datos del CLI para
        # su utilización en otras funciones. Es importante recordar que estos ficheros txt deben
        #ser borados manualmente cada vez que se apaga la red o se cambia al TGMS.            

        # Se muestra un mensaje al usuario informando que la configuración se ha restablecido
        # correctamente y se le pregunta si desea ver dicha configuración.
        print("El servicio de Internet y Video configurado anteriormente ha sido restablecido.\n")
        print("¿Desea ver la configuración cargada? Responda SI o NO") 
        true_respuesta=0
        while true_respuesta == 0:
            respuesta = input()
            # En caso de que quiera ver la configuración, se llama a la función ver_configuración
            if respuesta == "SI":
                ver_configuracion(MAC_ONU)
                true_respuesta = 1
            elif respuesta == "NO":
                print("\n")
                true_respuesta = 1
            else:
                print("Vuelva a introducir su respuesta (SI o NO)")
        
    return

# FUNCIÓN MAIN DEL PROGRAMA
# Desde aquí, se irán llamando a las funciones definidas arriba en función de lo que
# escoja el usuario.
# En primer lugar, se le da la bienvenida al programa de gestión y se le pide la dirección MAC
# de la ONU. Este será siempre el primer paso: el usuario deberá introducir la dirección MAC de
# la ONU que quiere manejar y a partir irá seleccionando qué hacer.   
print("Bienvenido al programa de gestión de la red GPON.")
print("A continuación, introduzca la número de serie de la ONU y la acción que desea llevar a cabo.\n")
# La función get_ID_ONU() es llamada al principio del programa para almacenar en el vector
# MAC las direcciones MAC de todas las ONUs que están conectadas a la red GPON.
MAC = get_ID_ONU()

# Este flag marca el fin del programa. Cuando se activa, se finaliza el bucle y 
# se sale del programa.
true_FIN = 0
while true_FIN == 0:
    cont=0
    true_MAC = 0 
    # Cada vez que se termina de realizar una acción con una ONU, se vuelve a este punto
    # en el que se pide de nuevo una dirección MAC para trabajar con una ONU.
    # La dirección MAC que introduce el usuario se compara con las del vector MAC. Mientras
    # no introduzca una reconocida por el programa, se le seguirá pidiendo de nuevo la MAC.
    # Esto es lo que se denominará menú principal.
    while true_MAC == 0:
        if cont == 0:
            print("Introduzca el número de serie de la ONU que va a configurar: ")
        else:
            print("Número de serie no reconocido. Introdúzcalo de nuevo: ") 
        direccion_MAC = input()
        cont = cont+1
        # Bucle que recorre el vector en el que se almacenan las direcciones MAC
        for i in range(0,len(MAC)):        
            if direccion_MAC == MAC[i]:
                print("\nEl número de serie ha sido reconocida correctamente.")
                true_MAC = 1
                ID_ONU = i
    
    # Estos son los nombres que toman los archivos de configuracion, en caso de existir.
    nombre_archivo1 = 'Servicio_Internet_ONU_MAC_' + direccion_MAC + '.txt'
    nombre_archivo2 = 'Servicio_Internet+Video_ONU_MAC_' + direccion_MAC + '.txt'
    # Si existe nombre_archivo1, la ONU tiene configurado un servicio de Internet.
    if path.exists(nombre_archivo1):
        print("\nLa ONU con número de serie " + direccion_MAC + " ya tiene configurado servicio de Internet.\n")
        # EL programa permite al usuario ver, modificar o borrar la configuración 
        # del servicio de Internet así volver al menú principal o finalizar el programa.
        true_opcion = 1
        true_eleccion = 0
        while true_eleccion == 0:
            print("¿Desea ver, cambiar o borrar la configuración? También puede volver al menú principal o finalizar el programa.")
            print("0 - Volver al menú principal")            
            print("1 - Ver configuración")
            print("2 - Cambiar configuración")
            print("3 - Borrar configuración")
            print("4 - Finalizar el programa")            
            cambio = input()
            # Si introduce un 0, el programa vuelve al menú principal donde se pide
            # la dirección MAC de la ONU sobre la que se quiere trabajar.
            if cambio == "0":
                print("\nHa seleccionado volver al menú principal.\n")
                true_eleccion = 1
            # Si introduce un 1, llama a la función ver_configuracion.
            elif cambio == "1":
                ver_configuracion(direccion_MAC)
            # Si introduce un 2, llama a la función modificar_configuracion.
            elif cambio == "2":
                modificar_configuracion(ID_ONU,direccion_MAC,nombre_archivo1)
            # Si introduce un 3, llama a la función borrar_configuracion.
            elif cambio == "3":  
                borrar_configuracion(ID_ONU,direccion_MAC,nombre_archivo1)
            # Si introduce un 4, el programa finaliza.
            elif cambio == "4":
                print("La configuración de la red GPON ha finalizado.")
                true_eleccion = 1
                true_FIN = 1                   
            # Si introduce un número no reconocido, se le pide que vuelva a introducirlo.
            else:
                print("\nOpción no válida. Vuelva a intentearlo.\n")            
            # Una vez se hayan ejecutado las funciones de ver, modificar o borrar
            # configuración, se le pregunta al usuario si desea seguir configurando la red o no.
            if cambio == "1" or cambio == "2" or cambio == "3":
                true_eleccion = 1
                true_respuesta=0
                while true_respuesta == 0:
                    print("¿Desea continuar configurando la red? Responda SI o NO")
                    respuesta = input()
                    # En caso de responder NO, el programa finaliza.
                    if respuesta == "NO":
                        print("La configuración de la red GPON ha finalizado.")
                        true_respuesta = 1
                        true_FIN = 1
                    elif respuesta == "SI":
                        # Si la respuesta es SI, el programa volverá al punto donde se
                        # pide la dirección MAC de la ONU a configurar.
                        print("\n")
                        true_respuesta = 1
                        break
                    else:
                        # Si la respuesta no es válida, se le vuelve a preguntar.
                        print("Vuelva a introducir su respuesta (SI o NO)")

    # Si existe nombre_archivo2, la ONU tiene configurado un servicio de Internet + Video.                   
    elif path.exists(nombre_archivo2):
        print("\nLa ONU con número de serie " + direccion_MAC + " ya tiene configurado servicio de Internet y vídeo.\n")
        # EL programa permite al usuario ver, modificar o borrar la configuración 
        # del servicio de Internet + Video así como volver al menú principal o 
        # finalizar el programa.        
        true_opcion = 1
        true_eleccion = 0
        while true_eleccion == 0:
            print("¿Desea ver, cambiar o borrar la configuración? También puede volver al menú principal o finalizar el programa.")
            print("0 - Volver al menú principal")
            print("1 - Ver configuración")
            print("2 - Cambiar configuración")
            print("3 - Borrar configuración")
            print("4 - Finalizar el programa")
            cambio = input()
            # Si introduce un 0, el programa vuelve al menú principal donde se pide
            # la dirección MAC de la ONU sobre la que se quiere trabajar.
            if cambio == "0":
                print("\nHa seleccionado volver al menú principal.\n")
                true_eleccion = 1
            # Si introduce un 1, llama a la función ver_configuracion.           
            elif cambio == "1":
                ver_configuracion(direccion_MAC)
            # Si introduce un 2, llama a la función modificar_configuracion.
            elif cambio == "2":
                modificar_configuracion(ID_ONU,direccion_MAC,nombre_archivo2)
            # Si introduce un 3, llama a la función borrar_configuracion.
            elif cambio == "3":  
                borrar_configuracion(ID_ONU,direccion_MAC,nombre_archivo2)
            # Si introduce un 1, finaliza el programa.
            elif cambio == "4":
                print("La configuración de la red GPON ha finalizado.")
                true_eleccion = 1
                true_FIN = 1  
            # Si introduce un número no reconocido, se le pide que vuelva a introducirlo.
            else:
                print("\nOpción no válida. Vuelva a intentearlo.\n")
            # Una vez se hayan ejecutado las funciones de ver, modificar o borrar
            # configuración, se le pregunta al usuario si desea seguir configurando la red o no.
            if cambio == "1" or cambio == "2" or cambio == "3":
                true_eleccion = 1
                true_respuesta=0
                while true_respuesta == 0:
                    print("¿Desea continuar configurando la red? Responda SI o NO")
                    respuesta = input()
                    # En caso de responder NO, el programa finaliza.
                    if respuesta == "NO":
                        print("La configuración de la red GPON ha finalizado.")
                        true_respuesta = 1
                        true_FIN = 1
                    # Si la respuesta es SI, el programa volverá al punto donde se
                    # pide la dirección MAC de la ONU a configurar.                    
                    elif respuesta == "SI":
                        print("\n")
                        true_respuesta = 1
                    else:
                        # Si la respuesta no es válida, se le vuelve a preguntar.
                        print("Vuelva a introducir su respuesta (SI o NO)")
    
    # En caso de que no existan los archivos de configuración, la ONU está sin configurar.
    # Por tanto, se marcará a 0 el flag true_opcion y el programa entrará en el siguiente bucle.
    else:
        true_opcion = 0
    
    # Se define la variable opción, que permitirá en el siguiente bucle llamar a una función u otra
    # dependiendo de la elección del usuario. Se le asigna un valor por defecto que no
    # coincida con ninguna opción de las que llaman a otras funciones (ahora mismo, los 
    # valores de 0 a 4 están utilizados). Ver opciones más abajo.
    opcion = 5
    # Si el programa entra en este bucle, quiere decir que la ONU está sin configurar. Por tanto,
    # el usuario podrá configurar para dicha ONU servicio de Internet, servicio de Internet + Video, 
    # cargar la última configuración activa (antes de que se apagara la red o se cambiara al TGMS),
    # volver al menú principal o finalizar el programa.
    while true_opcion == 0:
        print("\n¿Qué desea configurar en dicha ONU? Escoja la opción:")
        print("0 - Volver al menú principal")
        print("1 - Servicio de Internet")
        print("2 - Servicio de Internet + Vídeo")
        print("3 - Cargar la última configuración activa")
        print("4 - Finalizar el programa")
        opcion = input()
        opcion = int(opcion)
        if opcion == 0 or opcion == 1 or opcion == 2 or opcion == 3 or opcion == 4:
            true_opcion = 1
        else:
            # Si el usuario introduce un nº de opción no válido, se le vuelve a preguntar.
            print("Opción no válida. Vuelva a probar.")        

    # Si introduce un 0, el programa vuelve al menú principal donde se pide
    # la dirección MAC de la ONU sobre la que se quiere trabajar.    
    if opcion == 0:
        print("\nHa seleccionado volver al menú principal.\n")
    # Si el usuario introduce un 1, se va a configurar servicio de Internet.
    elif opcion == 1:
        print("\nConfiguración del servicio de Internet:")
        cont=0
        true_Internet = 0
        # Se le pide el nº de servicios. Este nº podría ser mayor que 2 pero realmente,
        # no tiene sentido configurar más de 2 servicios puesto que solo hay 2 VLAN
        # que proporcionen servicio a esta red. 
        while true_Internet == 0:
            if cont == 0:
                print("Número de servicios de datos:")
            else:
                print("Número de servicios no válido. Vuelva a probar:")
            cont = cont+1
            num_servicios_Internet = input()
            num_servicios_Internet = int(num_servicios_Internet)
            if num_servicios_Internet > -1 and num_servicios_Internet < 3:
                true_Internet = 1
        
        print("\n")
        # En caso de que el número de servicios sea mayor que 0, se llamará a la función
        # servicio_Internet.
        if num_servicios_Internet > 0: 
            servicio_Internet(ID_ONU,direccion_MAC,num_servicios_Internet)
        # Tanto si se han configurado 1 o más servicios de Internet, como si no se ha 
        # configurado ninguno, el programa pregunta al usuario si desea seguir configurando la red.
        true_respuesta=0
        while true_respuesta == 0:
            print("¿Desea continuar configurando la red? Responda SI o NO")
            respuesta = input()
            if respuesta == "NO":
                print("La configuración de la red GPON ha finalizado.")
                true_respuesta = 1
                true_FIN = 1
            elif respuesta == "SI":
                print("\n")
                true_respuesta = 1
            else:
                print("Vuelva a introducir su respuesta (SI o NO)")

    # Si el usuario introduce un 2, se va a configurar servicio de Internet+Video.                
    elif opcion == 2:
        print("\nConfiguración del servicio de Vídeo:")
        cont=0
        true_Video = 0
        # Se le pide el nº de servicios. Este nº podría ser mayor que 2 pero realmente,
        # no tiene sentido configurar más de 2 servicios puesto que solo hay 2 VLAN
        # que proporcionen servicio a esta red. 
        while true_Video == 0:
            if cont == 0:
                print("Número de servicios de vídeo:")
            else:
                print("Número de servicios no válido. Vuelva a probar:")
            cont = cont+1
            num_servicios_Video = input()
            num_servicios_Video = int(num_servicios_Video)
            if num_servicios_Video > -1 and num_servicios_Video < 3:
                true_Video = 1
        
        print("\n")
        # En caso de que el número de servicios sea mayor que 0, se llamará a la función
        # servicio_Video.        
        if num_servicios_Video > 0: 
            servicio_Video(ID_ONU,direccion_MAC,num_servicios_Video)
        # Tanto si se han configurado 1 o más servicios de Internet+Video, como si no se ha 
        # configurado ninguno, el programa pregunta al usuario si desea seguir configurando la red.        
        true_respuesta=0
        while true_respuesta == 0:
            print("¿Desea continuar configurando la red? Responda SI o NO")
            respuesta = input()
            if respuesta == "NO":
                print("La configuración de la red GPON ha finalizado.")
                true_respuesta = 1
                true_FIN = 1
            elif respuesta == "SI":
                print("\n")
                true_respuesta = 1
            else:
                print("Vuelva a introducir su respuesta (SI o NO)")
                
    # Si el usuario introduce un 3, se va a cargar la última configuración activa (si 
    # es que había alguna) antes de apagar la red o cambiar al modo de gestión TGMS      
    elif opcion == 3:
        cargar_configuracion(ID_ONU,direccion_MAC)
        # Una vez ejecutada la función, el programa pregunta al usuario si desea seguir configurando la red.
        true_respuesta=0
        while true_respuesta == 0:
            print("¿Desea continuar configurando la red? Responda SI o NO")
            respuesta = input()
            if respuesta == "NO":
                print("La configuración de la red GPON ha finalizado.")
                true_respuesta = 1
                true_FIN = 1
            elif respuesta == "SI":
                print("\n")
                true_respuesta = 1
            else:
                print("Vuelva a introducir su respuesta (SI o NO)")
     
    # Si el usuario introduce un 4, finaliza el programa.            
    elif opcion == 4:
        print("La configuración de la red GPON ha finalizado.")
        true_FIN = 1